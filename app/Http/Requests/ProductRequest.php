<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required|numeric',
            'selling_price' => 'sometimes|nullable|numeric',//digits
            'energy' => 'sometimes|nullable|numeric',
            'protein' => 'sometimes|nullable|numeric',
            'dietary_fiber' => 'sometimes|nullable|numeric',
            'carbohydrates' => 'sometimes|nullable|numeric',
            'sugar' => 'sometimes|nullable|numeric',
            'total_fat' => 'sometimes|nullable|numeric',
            'saturated_fat' => 'sometimes|nullable|numeric',
            'fiber' => 'sometimes|nullable|numeric',
            'calories' => 'sometimes|nullable|numeric',
            'ingredients' => 'sometimes|nullable'
        ];

        if (request()->isMethod('POST')) {
            $rules['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg:max:1024';
            $rules['thumbnails'] = 'required';
            $rules['thumbnails.*'] = 'mimes:jpeg,png,jpg,gif,svg:max:512';
        }

        return $rules;
    }
}
