<?php


    namespace App\Http\Controllers\API;


    trait APIResponser
    {
        public function successResponse($data)
        {
            return response()->json(['status'=>'success', 'data'=>$data]);
        }

        public function errorResponse($errorData)
        {
            return response()->json(['status'=>'failure', 'errors'=>$errorData]);
        }
    }
