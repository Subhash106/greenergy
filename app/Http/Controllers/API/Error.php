<?php


    namespace App\Http\Controllers\API;


    class Error
    {
        const CODE_MISSION_CREDENTIALS = 100001;
        const CODE_INVALID_CREDENTIAL = 100002;
        const CODE_INVALID_PARAMETER = 200002;
        const CODE_MISSION_PARAMETER = 200001;
        const CODE_UNAUTHORIZED = 400000;
    }
