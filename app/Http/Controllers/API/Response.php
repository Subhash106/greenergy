<?php


    namespace App\Http\Controllers\API;


    class Response
    {
        const HTTP_OK = 200;
        const HTTP_CREATED = 201;
        const HTTP_FORBIDDEN = 403;
        const HTTP_NOT_FOUND = 404;
        const HTTP_BAD_REQUEST = 400;
        const HTTP_UNAUTHORIZED = 401;
        const HTTP_INTERNAL_SERVER_ERROR = 500;

        const STATUS_MESSAGE_SUCCESS = "success";
        const STATUS_MESSAGE_FAILURE = "failure";
    }
