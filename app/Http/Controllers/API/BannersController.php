<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin\Banner;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BannersController extends Controller
{
    public function index()
    {
        return response()->json([
            'upcomingDesigns' => Banner::with('bannerCategory')->where('banner_category_id', 1)->get(),
            'newTrends' => Banner::with('bannerCategory')->where('banner_category_id', 2)->get(),
        ]);
    }
}
