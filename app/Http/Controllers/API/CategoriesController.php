<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use App\Models\Admin\Product;
use App\Models\Admin\WishList;
use App\User;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index($mainCategoryId, $userId)
    {
        $wishlists = WishList::where('user_id', $userId)->get()->pluck('product_id')->toArray();
        $categories = Category::where('main_category_id', $mainCategoryId)->get()->toArray();
        //array_unshift($categories, ['id' => 'ALL_'.$mainCategoryId, 'name' => 'All']);
        $allProducts = Product::with('image', 'thumbnails')->where('main_category_id', $mainCategoryId)->get();

        foreach ($allProducts as $product){
            if(in_array($product->id, $wishlists)){
                $product->addedToWishList = true;
            }else{
                $product->addedToWishList = false;
            }
        }

        return response()->json(['subcategories' => $categories, 'allproducts' => $allProducts]);
    }

    public function products($categoryId, $userId)
    {
        $wishlists = WishList::where('user_id', $userId)->get()->pluck('product_id')->toArray();
        if(strpos($categoryId, 'ALL') !== false){
            $mainCategory = explode('_', $categoryId);
            $mainCategoryId = $mainCategory[1];
            $products = Product::with('image', 'thumbnails')->where('main_category_id', $mainCategoryId)->get();
        }else{
            $products = Product::with('image', 'thumbnails')->where('category_id', $categoryId)->get();
        }

        foreach ($products as $product){
            if(in_array($product->id, $wishlists)){
                $product->addedToWishList = true;
            }else{
                $product->addedToWishList = false;
            }
        }

        return $products;
    }
}
