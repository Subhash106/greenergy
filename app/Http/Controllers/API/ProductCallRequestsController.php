<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIResponser;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\ProductCallRequest;

class ProductCallRequestsController extends Controller
{
    use APIResponser;

    public function  sendCallRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            $responseData = $this->errorsToArray($validator->errors()->toArray());
            return $this->errorResponse($responseData);
        }

        $product =Product::find($request->product_id);
        $user = User::find($request->user_id);


        if(!($product && $user))
        {
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'Something is wrong with product or user';
            return $this->errorResponse($responseData);
        }

        $input = $request->all();
        $call_request = ProductCallRequest::create($input);

        if ($call_request) {
            $responseData['message'] = 'Your call request is submitted successfully. Our team will contact you soon';
            return $this->successResponse($responseData);
        }
        $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
        $responseData['errorMessage'] = 'Something went wrong. Call request is not received';
        return $this->errorResponse($responseData);
    }

    private function errorsToArray($validator_error)
    {
        $all_errors = [];
        foreach ($validator_error as $key => $errors) {
            foreach ($errors as $error) {
                $field_error['errorCode'] = Error::CODE_MISSION_PARAMETER;
                $field_error['errorMessage'] = $error;
                $all_errors[] = $field_error;
            }
        }
        return $all_errors;
    }

    public function CallbackRequestsByUser($id)
    {
        $user = User::find($id);
        if ($user) {
            $products_for_callback = $user->productsForCallback;
            if ($products_for_callback->count()) {
                $responseData = $products_for_callback->toArray();
                return $this->successResponse($responseData);
            }
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'No product enquiry found for callback';
            return $this->errorResponse($responseData);
        }

        $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
        $responseData['errorMessage'] = 'User does not exist';
        return $this->errorResponse($responseData);
    }

}
