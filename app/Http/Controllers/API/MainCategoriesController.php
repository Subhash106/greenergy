<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin\MainCategory;
use Illuminate\Http\Request;

class MainCategoriesController extends Controller
{
    public function index()
    {
        return MainCategory::all();
    }
}
