<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\WishList;
use App\Models\Admin\Product;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\API\APIResponser;

class WishListController extends Controller
{
    use APIResponser;

    public function addProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'product_id' => 'required'
        ]);

        if ($validator->fails()) {
            $responseData = $this->errorsToArray($validator->errors()->toArray());
            return $this->errorResponse($responseData);
        }

        $product = Product::find($request->product_id);
        $user = User::find($request->user_id);


        if (!($product && $user)) {
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'Something is wrong with product or user';
            return $this->errorResponse($responseData);
        }

        $input = $request->all();
        $is_already_added = WishList::where('user_id', $user->id)->where('product_id', $product->id)->get();
        if (!$is_already_added->count()) {
            $wishlist = WishList::create($input);

            if ($wishlist) {
                $responseData['message'] = 'Product is successfully added to your wishlist';
                return $this->successResponse($responseData);
            }
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'Something went wrong. Couldn\'t add product to wishlist';
            return $this->errorResponse($responseData);
        }
        $responseData['errorMessage'] = 'Already added to wishlist';
        return $this->errorResponse($responseData);

    }

    public function removeProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'product_id' => 'required'
        ]);

        if ($validator->fails()) {
            $responseData = $this->errorsToArray($validator->errors()->toArray());
            return $this->errorResponse($responseData);
        }

        $product = Product::find($request->product_id);
        $user = User::find($request->user_id);


        if (!($product && $user)) {
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'Something is wrong with product or user';
            return $this->errorResponse($responseData);
        }

        $wishlist = WishList::where('product_id', $request->product_id)
            ->where('user_id', $request->user_id)
            ->first();
        if ($wishlist) {
            if ($wishlist->delete()) {
                $responseData['message'] = 'Product is successfully removed from wishlist';
                return $this->successResponse($responseData);
            }
            $responseData['message'] = 'Something Went wrong. Couldn\'t remove from wishlist';
            return $this->errorResponse($responseData);
        }
        $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
        $responseData['errorMessage'] = 'Wishlist not found';
        return $this->errorResponse($responseData);
    }

    public function getAllProducts(Request $request, int $user_id)
    {
        $wishlist = WishList::with('product.image')->where('user_id', $user_id)->get();

        $responseData['total_wishlist_item'] = $wishlist->count();

        if ($responseData['total_wishlist_item'])
            $responseData['wishlist'] = $wishlist;

        return $this->successResponse($responseData);
    }

    private function errorsToArray($validator_error)
    {
        $all_errors = [];
        foreach ($validator_error as $key => $errors) {
            foreach ($errors as $error) {
                $field_error['errorCode'] = Error::CODE_MISSION_PARAMETER;
                $field_error['errorMessage'] = $error;
                $all_errors[] = $field_error;
            }
        }
        return $all_errors;
    }
}
