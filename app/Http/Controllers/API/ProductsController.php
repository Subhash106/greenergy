<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use Illuminate\Http\Request;
use App\Models\Admin\Product;
use App\Models\Admin\WishList;

class ProductsController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('image', 'thumbnails')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::with('image', 'thumbnails')->where('id', $id)->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id, $userId)
    {
        $wishlists = WishList::where('user_id', $userId)->get()->pluck('product_id')->toArray();
        $product = Product::with('image', 'thumbnails')->where('id', $id)->first();

        if(in_array($product->id, $wishlists)){
            $product->addedToWishList = true;
        }else{
            $product->addedToWishList = false;
        }

        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProductOnCategoryBasis($category_id)
    {
        $products = Category::with('products')->find($category_id);
        if($products) {
            return response()->json(['status' => 'success', 'data'=>[$products]]);
        }
        return response()->json(['status' => 'failure', 'message'=>['errorCode'=>402, 'message'=>'resource not found']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Featured products
     *
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function featured($userId)
    {
        $wishlists = WishList::where('user_id', $userId)->get()->pluck('product_id')->toArray();
        $products = Product::with('image', 'thumbnails')->whereIn('id', range(1, 10))->get();

        foreach ($products as $product){
            if(in_array($product->id, $wishlists)){
                $product->addedToWishList = true;
            }else{
                $product->addedToWishList = false;
            }
        }

        return $products;
    }

    public function searchProduct(Request $request)
    {
        if (isset($request->search_query)) {
            $search_keys = explode(' ', $request->search_query);
            if (count($search_keys)) {
                $products = null;
                foreach ($search_keys as $keys) {
                    if (is_numeric($keys)) {
                        $search_result = Product::where('price', $keys)->with('image', 'thumbnails')->get();
                    } else {
                        $search_result = Product::where('name', 'LIKE', "%{$keys}%")->with('image', 'thumbnails')->get();
                    }
                    if (!is_null($products)) {
                        $products = $products->merge($search_result);
                    } else {
                        $products = $search_result;
                    }
                }
            }

            $wishlists = WishList::where('user_id', $request->userId)->get()->pluck('product_id')->toArray();
            foreach ($products as $product){
                if(in_array($product->id, $wishlists)){
                    $product->addedToWishList = true;
                }else{
                    $product->addedToWishList = false;
                }
            }

            return response()->json(['status' => 'success', 'data' => $products]);
        }
        return response()->json(['status' => 'failure', 'message' => ['errorCode' => 402, 'message' => 'search key is not provided']]);
    }
}
