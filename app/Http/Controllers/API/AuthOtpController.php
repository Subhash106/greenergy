<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIResponser;
use App\Http\Controllers\Controller;
use App\Models\Admin\LoginHistory;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Repositories\SendMessage\SendMessageRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthOtpController extends Controller
{
    use APIResponser;

    protected $messageService;
    protected $users;

    public function __construct(
        SendMessageRepositoryContract $messageService,
        UserRepositoryContract $users)
    {
        $this->messageService = $messageService;
        $this->users = $users;
    }

    public function getOtp(Request $request)
    {
        $responseData = [];
        $phone = $request->phone;
        $user = User::where('phone', $phone)->first();

        if ($user) {
            if ($user->status) {
                $otp = rand(1000, 9999);

                $login_history = LoginHistory::create(['user_id' => $user->id, 'otp' => $otp]);

                $sms_response = $this->messageService->send([$phone], "OTP for login MBJ app " . $otp);
                $sms_response = json_decode($sms_response);
                if ($sms_response->status === 'success') {
                    $login_history->is_sms_sent = true;
                    $login_history->save();
                    $responseData['message'] = 'An OTP is sent to ' . $phone;
                    return $this->successResponse($responseData);
                }
                return $this->errorResponse($sms_response->errors);
            }

            $responseData['errorCode'] = Error::CODE_UNAUTHORIZED;
            $responseData['errorMessage'] = 'You are not an active user. Please contact admin';
            return $this->errorResponse($responseData);
        }

        $responseData['errorCode'] = Error::CODE_INVALID_PARAMETER;
        $responseData['errorMessage'] = 'Phone number provided is not registered';
        return $this->errorResponse($responseData);
    }

    public function verifyOtpToLogin(Request $request)
    {
        $responseData = [];
        $phone = $request->phone;
        $otp = $request->otp;

        $loginHistory = LoginHistory::with('user')->where('otp', $otp)->where('is_used', 0)->first();

        if($loginHistory)
        {
            $user = $loginHistory->user;
        }else{
            $responseData['errorCode'] = Error::CODE_INVALID_CREDENTIAL;
            $responseData['errorMessage'] = 'Invalid OTP';
            return $this->errorResponse($responseData);
        }

        //$user = User::where('phone', $phone)->first();
        if ($user && $user->status) {
            $login_history = LoginHistory::where('user_id', $user->id)->where('is_used', false)->latest()->first();
            if ($login_history) {
                if ($login_history->otp == $otp) {
                    try {
                        if ($token = auth('api')->login($user)) {
                            $login_history->is_used = true;
                            $login_history->save();
                            $responseData['user'] = $user;
                            $responseData['token'] = ($this->respondWithToken($token))->original;
                            return $this->successResponse($responseData);
                        }
                    } catch (JWTException $e) {
                        $responseData['errorCode'] = $e->getCode();
                        $responseData['errorMessage'] = $e->getMessage();
                        return $this->errorResponse($responseData);
                    }
                }

                $responseData['errorCode'] = Error::CODE_INVALID_CREDENTIAL;
                $responseData['errorMessage'] = 'Invalid OTP';
                return $this->errorResponse($responseData);
            }

            $responseData['errorCode'] = Error::CODE_INVALID_CREDENTIAL;
            $responseData['errorMessage'] = 'Invalid OTP';
            return $this->errorResponse($responseData);
        }

        $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
        $responseData['errorMessage'] = 'Phone number provided is not registered';
        return $this->errorResponse($responseData);
    }

    public function signUp(Request $request)
    {
        $responseData = [];
        $rules = [
            'name' => 'required',
            'dob' => 'required|date',
            'phone' => 'required|numeric|unique:users,phone',
            'anniversary_date' => 'required|date'
        ];
        if(isset($request->email))
            $rules['email'] = 'email|unique:users,email';
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $responseData = $this->errorsToArray($validator->errors()->toArray());
            return $this->errorResponse($responseData);
        }
        $input = $request->all();
        $input['email'] = !isset($request->email) ? $request->name . $request->phone . '@gmail.com' : $request->email;
        $input['status'] = false;
        $input['dob'] = date('Y-m-d H:i:s', strtotime($request->dob));
        $input['anniversary_date'] = date('Y-m-d H:i:s', strtotime($request->anniversary_date));
        $input['password'] = Hash::make(rand(100000, 999999));

        $user = $this->users->create($input);
        if ($user) {
            $responseData['message'] = 'You are registered now your account will be active shortly';
            return $this->successResponse($responseData);
        } else {
            $responseData['errorCode'] = Error::CODE_MISSION_PARAMETER;
            $responseData['errorMessage'] = 'Something went wrong. user not created';
            return $this->errorResponse($responseData);
        }
    }

    private function errorsToArray($validator_error)
    {
        $all_errors = [];
        foreach ($validator_error as $key => $errors) {
            foreach ($errors as $error) {
                $field_error['errorCode'] = Error::CODE_MISSION_PARAMETER;
                $field_error['errorMessage'] = $error;
                $all_errors[] = $field_error;
            }
        }
        return $all_errors;
    }

    public function guard()
    {
        return Auth::guard('api');
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

}
