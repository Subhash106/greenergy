<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validation_rules = [
            'phone' => 'required',
            'password' => 'required',
        ];
        $validation_result = $this->customValidation($request->all(), $validation_rules);
        if($validation_result['status']!='success')
           return response()->json($validation_result);

        $credentials = $request->only('phone', 'password');
        try {
            if ($token = $this->guard()->attempt($credentials)) {
                return $this->respondWithToken($token);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    public function register(Request $request)
    {
        $validation_rules = [
            'phone' => 'required|digits:10|unique:users,phone',
            'name' => 'required|max:255',
            'password' => 'required',
        ];
        if (isset($request->email))
            $validation_rules['email'] = 'email|unique:users,email';

        $validation_result = $this->customValidation($request->all(), $validation_rules);
        if ($validation_result['status'] != 'success')
            return response()->json($validation_result);
        $email = !isset($request->email) ? $request->name . $request->phone . '@gmail.com' : $request->email;
        $user = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $email,
            'password' => bcrypt($request->password),
        ]);
        if ($user) {
            $token = $this->login($request);
            $data = $token->getData('true');
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['phone'] = $user->phone;
            return response()->json(['status' => 'success', 'data' => $data]);
        }
        return response()->json(['status' => 'failure', 'errors' => [['errorCode' => '40001', 'message' => 'Registration unsuccessful... please try again']]]);

    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    protected function customValidation($data, $validation_rule)
    {
        $validator = \Validator::make($data, $validation_rule);
        if ($validator->fails()) {
            $all_errors = [];
            foreach ($validator->errors()->toArray() as $key => $errors) {
                $field_error['errorCode'] = 40001;
                $field_error['message'] = $errors[0];
                $all_errors[] = $field_error;
            }
            return ['status' => 'failure', 'errors' => $all_errors];
        }
        return ['status'=>'success'];
    }

}
