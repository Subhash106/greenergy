<?php

namespace App\Http\Controllers;

use App\Models\Admin\Category;
use App\Models\Admin\MainCategory;
use App\Models\Admin\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $from_the_farm = MainCategory::where('slug', 'from-the-farm')->with(['categories' => function($query){
            $query->with('products')->get();
        }])->first();

        $organic_products = MainCategory::where('slug', 'organic-products')->with(['categories' => function($query){
            $query->with('products')->get();
        }])->first();

        return view('greenergy.greenergy-latest', compact('from_the_farm','organic_products' ));
    }

    public function faq()
    {
        return view('greenergy.faq');
    }

    public function accountSettings()
    {
        return view('greenergy.account-settings');
    }

    public function cart()
    {
        return view('greenergy.cart');
    }

    public function myOrder()
    {
        return view('greenergy.my-order');
    }

    public function orderHistory()
    {
        return view('greenergy.order-history');
    }

    public function orderTracking()
    {
        return view('greenergy.order-tracking');
    }

    public function paymentMethods()
    {
        return view('greenergy.payment-methods');
    }

    public function wallet()
    {
        return view('greenergy.wallet');
    }

    public function wishlist()
    {
        return view('greenergy.wishlist');
    }

    public function getProduct(Product $product)
    {
        if (request()->ajax()) {
            if (!is_null($product)) {
                $response['status'] = 'success';
                $product_details = $product->toArray();
                $product_details['image'] = isset($product->image->url)? $product->image->url: asset('default-image.jpg');
                $product_details['category'] = $product->category->name;
                $product_details['packages'] = $product->packages->toArray();
                $response['data'] = $product_details;
            } else {
                $response['status'] = 'failed';
                $response['message'] = 'Product Not Found';
            }
        } else {
            $response['status'] = 'failed';
            $response['error'] = 'invalid request type';
        }
        return response()->json($response);
    }

}
