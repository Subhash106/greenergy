<?php

namespace App\Http\Controllers\Greenergy;

use App\Http\Controllers\Controller;
use App\Models\Greenergy\CallRequest;
use Illuminate\Http\Request;

class CallRequestsController extends Controller
{
    public function index()
    {
        $call_requests = CallRequest::paginate(10);

        return view('greenergy.admin.call_requests.index', compact('call_requests'));
    }


    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'phone' => 'required|digits:10|unique:call_requests',
            'email' => 'required|email|unique:call_requests',
            'city' => 'required',
            'pincode' => 'required|digits:6',
            //'call_time' => 'required'
        ]);
        //$input['call_time'] = date('Y-m-d H:i:s', strtotime($input['call_time']));
        CallRequest::create($input);

        return redirect('/')->with('success', 'Thanks for submitting your query. Someone from our team will contact you shortly.');
    }
}
