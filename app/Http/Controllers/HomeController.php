<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Product;
use App\Models\Admin\ProductCallRequest;
use App\Models\Greenergy\CallRequest;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_users = User::all()->count();
        $total_call_requests = CallRequest::all()->count();
        $total_product_call_requests = ProductCallRequest::all()->count();
        $total_products = Product::all()->count();
        return view('home', compact('total_users', 'total_call_requests', 'total_product_call_requests', 'total_products'));
    }
}
