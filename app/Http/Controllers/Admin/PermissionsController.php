<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Role;
use Illuminate\Http\Request;
use App\Models\Admin\Permission;
use App\Repositories\Permission\PermissionRepositoryContract;
use Illuminate\Support\Str;

class PermissionsController extends Controller
{
    protected $permission;

    public function __construct(PermissionRepositoryContract $permission)
    {
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->permission->paginate(10);

        return view('admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['slug'=>Str::slug($request->permission)]);
        $input = $request->validate([
            'permission' => 'required',
            'slug' => 'required|unique:permissions,slug'
        ]);

        $this->permission->create($input);

        return redirect(route('permissions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = $this->permission->getById($id);
        $assigned_roles = $permission->roles()->select('roles.id','role')->get();
        $roles = Role::select('id', 'role')->get();
        return view('admin.permissions.show', compact('permission', 'roles', 'assigned_roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = $this->permission->getById($id);
        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['slug'=>Str::slug($request->permission)]);
        $input = $request->validate([
            'permission' => 'required',
            'slug' => 'required|unique:permissions,slug,'.$id
        ]);

        $this->permission->update($request->all()+['slug'=>Str::slug($request->permission)], $id);

        return redirect(route('permissions.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->permission->delete($id);

        return redirect(route('permissions.index'));
    }

    public function assignToRoles(Request $request, $permission_id)
    {
        $permission = Permission::find($permission_id);
        $permission->roles()->sync($request->roles);
        return back();
    }

    public function removeRole($id)
    {
        \DB::table('permission_role')->where('id', $id)->delete();

        return back();
    }
}
