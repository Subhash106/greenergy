<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\ProductCallRequest;

class ProductCallRequestsController extends Controller
{
    public function callRequestList()
    {
        $call_requests = ProductCallRequest::paginate(10);

        return view('admin.product-call-request.index', compact('call_requests'));
    }
}
