<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Permission;
use Illuminate\Http\Request;
use App\Models\Admin\Role;
use App\Repositories\Role\RoleRepositoryContract;
use Illuminate\Support\Str;

class RolesController extends Controller
{
    protected $role;

    public function __construct(RoleRepositoryContract $role)
    {
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->role->paginate(10);

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['slug'=>Str::slug($request->role)]);
        $input = $request->validate([
            'role' => 'required',
            'slug' => 'required|unique:roles,slug'
        ]);

        $this->role->create($input);

        return redirect(route('roles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->role->getById($id);
        $assigned_permissions = $role->permissions()->select('permissions.id','permission')->get();
        $permissions = Permission::select('id', 'permission')->get();
        return view('admin.roles.show', compact('role', 'permissions', 'assigned_permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role->getById($id);
        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['slug'=>Str::slug($request->role)]);
        $input = $request->validate([
            'role' => 'required',
            'slug' => 'required|unique:roles,slug,'.$id
        ]);

        $this->role->update($request->all()+['slug'=>Str::slug($request->role)], $id);

        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->role->delete($id);

        return redirect(route('roles.index'));
    }

    public function assignPermissions(Request $request, $role_id)
    {
        $role = Role::find($role_id);
        $role->permissions()->sync($request->permissions);
        return back();
    }

    public function removePermission($id)
    {
        \DB::table('permission_role')->where('id', $id)->delete();

        return back();
    }
}
