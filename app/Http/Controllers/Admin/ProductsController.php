<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Image;
use App\Models\Admin\Package;
use App\Models\Admin\Product;
use App\Models\Admin\Thumbnail;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Repositories\Category\CategoryRepositoryContract;
use App\Repositories\Product\ProductRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\MainCategory;
use App\Http\Requests\ProductRequest;

class ProductsController extends Controller
{
    protected $product;
    protected $category;

    public function __construct(ProductRepositoryContract $product, CategoryRepositoryContract $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index()
    {
        Log::info('Test log', ['id' => 10]);

        $products = Product::with('image')->paginate(10);
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $categories = $this->category->getAll();

        return view('admin.products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $input = $request->all();

        $this->product->create($input);

        return redirect(route('products.index'));
    }

    public function show($id)
    {
        $packages = Package::all();

        $product = $this->product->getById($id);

        return view('admin.products.show', compact('product', 'packages'));
    }

    public function edit($id)
    {
        $product = $this->product->getById($id);
        $categories = $this->category->getAll();
        $main_categories = MainCategory::select('id', 'name')->get();
        return view('admin.products.edit', compact('product', 'categories', 'main_categories'));
    }

    public function update(ProductRequest $request, int $id)
    {
        $input = $request->all();

        $this->product->update($input, $id);

        return redirect(route('products.index'));
    }

    public function destroy(int $id)
    {
        $this->product->delete($id);

        return redirect(route('products.index'));
    }

    public function publish($id)
    {
        //if (!Auth::user()->hasPermission('can-publish-product')) {
        if (!Auth::user()->hasRole('super-admin')) {
            return abort('403', 'you don\'t have permission to perform this action');
        }
        $this->product->publish($id);

        return redirect(route('products.index'));
    }

    public function unPublish($id)
    {
        if (!Auth::user()->hasRole('super-admin')) {
            return abort('403', 'you don\'t have permission to perform this action');
        }

        $this->product->unPublish($id);

        return redirect(route('products.index'));
    }

    public function addPackages(Request $request, Product $product)
    {
        $request->validate([ 'packages' => 'required|array|min:1'],
        [
            'packages.required' => 'Please select a package',
            'packages.array' => 'Something is wrong with package choice',
        ]);

        $product->packages()->syncWithoutDetaching($request->packages);

        Session::flash('success', 'Package added successfully');

        return back();
    }

    public function removePackage(Request $request, Product $product)
    {
        $request->validate([ 'package_id' => 'required']);

        $product->packages()->detach($request->package_id);

        Session::flash('success', 'Package removed successfully');

        return back();

    }

    public function bulkUpload(Request $request)
    {
        $categories = $this->category->getAll();
        return view('admin.products.bulk-upload', compact('categories'));
    }

    public function storeBulkUpload(Request $request)
    {
        $file = fopen($request->file('products'), "r");
        $required_fields = [
            'main_category_id', 'category_id', 'name', 'price', 'primary_sku', 'secondary_sku',
            'image', 'gross_weight', 'design_code', 'net_weight', 'tag_price', 'pieces',
            'article_necklace', 'pearl_weight', 'kundan_stones', 'carat', 'diamond_set', 'diamond_weight',
            'diamond_pieces', 'color_stone_weight', 'color_stone_pieces', 'polki_weight', 'polki_pieces'
        ];
        $flag = false;
        $no_of_records = NULL;
        while (!feof($file)) {
            if ($flag) {
                $record = (array)fgetcsv($file);
                $required_data = array_merge(array_slice($record, 0, 7), array_slice($record, 12));

                // creating associative array to create request
                if (count($required_data) == count($required_fields)) {
                    $product_details = array_combine($required_fields, $required_data);
                    if ($this->urlCheck($product_details['image']))
                        $product_details['image'] = $this->uploadFile($product_details['image']);
                    $product_details['thumbnails'] = array();
                    for ($i = 7; $i <= 11; $i++)
                        if (!empty($record[$i]) && $this->urlCheck($record[$i]))
                            $product_details['thumbnails'][] = $this->uploadFile($record[$i]);

                    $product_details = new Request($product_details);
                    $validator = Validator::make($product_details->all(), [
                        'name' => 'required',
                        'main_category_id' => 'required',
                        'category_id' => 'required',
                        'price' => 'required|numeric',
                        'primary_sku' => 'required',
                        'secondary_sku' => 'required',
                        'gross_weight' => 'sometimes|nullable|numeric',
                        'design_code' => 'sometimes|nullable',
                        'net_weight' => 'sometimes|nullable|numeric',
                        'tag_price' => 'sometimes|nullable|numeric',
                        'pieces' => 'sometimes|nullable|numeric',//digits
                        'article_necklace' => 'sometimes|nullable',
                        'pearl_weight' => 'sometimes|nullable|numeric',
                        'kundan_stones' => 'sometimes|nullable|numeric',//digits
                        'carat' => 'sometimes|nullable|numeric',//digits
                        'diamond_set' => 'sometimes|nullable|numeric',//digits
                        'diamond_weight' => 'sometimes|nullable|numeric',
                        'diamond_pieces' => 'sometimes|nullable|numeric',//digits
                        'color_stone_weight' => 'sometimes|nullable|numeric',
                        'color_stone_pieces' => 'sometimes|nullable',
                        'polki_weight' => 'sometimes|nullable|numeric',
                        'polki_pieces' => 'sometimes|nullable',
                    ], ['image.required' => 'please check image url']);

                    if ($validator->fails()) {
                        fclose($file);
                        dd($validator->errors());
                    } else {
                        // saving product
                        try {
                            if ($product_details->image == null || $product_details->thumbnails[0] == null) {
                                Session::flash('error', 'main image or first thumbnail is provided');
                                return back();
                            }
                            $this->product->create($product_details->toArray());
                        } catch (\Illuminate\Database\QueryException $ex) {
                            Session::flash('error', 'Please check the category in uploaded csv file');
                            return back();
                        }
                    }
                } else {
                    if (!empty($required_data[0])) {
                        fclose($file);
                        Session::flash('error', 'Fields mismatch');
                        return redirect()->back();
                    }
                }
                // uploading image and getting its name
            } else {
                $flag = true;
                $no_of_records = count(fgetcsv($file));
            }
        }
        fclose($file);
        Session::flash('success', 'Products Uploaded successfully');
        return redirect()->back();
    }

    protected function uploadFile($url)
    {
        $info = pathinfo($url);
        copy($url, storage_path('app/public/' . $info['basename']));
        $uploaded_file = new UploadedFile(storage_path('app/public/' . $info['basename']), $info['basename']);
        return $uploaded_file;
    }

    protected function urlCheck($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
            return true;
        }
        return false;
    }

    public function downloadExcelTemplate()
    {
        if (file_exists(public_path('Template.csv'))) {
            return response()->download(public_path('Template.csv'));
        } else {
            Session::flash('error', 'Template Does not exist');
            return back();
        }
    }

}
