<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\State;
use Illuminate\Http\Request;
use App\Repositories\GoldPrice\GoldPriceRepositoryContract;


class GoldPriceController extends Controller
{
    protected $gold_price;

    public function __construct(GoldPriceRepositoryContract $gold_price)
    {
        $this->gold_price = $gold_price;
    }

    public function index()
    {
        $gold_prices = $this->gold_price->paginate(10);

        return view('admin.gold-price.index', compact('gold_prices'));
    }

    public function create()
    {
        $states = State::all();
        return view('admin.gold-price.create', compact('states'));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'price' => 'required',
            'state_id' => 'required'
        ]);

        $this->gold_price->create($input);

        return redirect(route('gold-price-list'));
    }

}
