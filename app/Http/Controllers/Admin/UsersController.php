<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Http\Request;
use App\User;
use App\Repositories\SendMessage\SendMessageRepositoryContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * @var UserRepositoryContract
     */
    protected $users;
    protected $message;

    public function __construct(UserRepositoryContract $users, SendMessageRepositoryContract $message)
    {
        $this->users = $users;
        $this->message = $message;
    }

    /*
     * show users list
     * */
    public function index(){
        $users = $this->users->paginate(10);

        return view('admin.users.index', compact('users'));
    }

    /*
     * enable user
     * */

    public function enable($id)
    {
        if (!Auth::user()->hasRole('super-admin')) {
            return abort('403', 'you don\'t have permission to perform this action');
        }
        if (Auth::id() == $id) {
            Session::flash('error', 'You cannot disable yourself');
            return back();
        }
        if (!Auth::user()->hasRole('super-admin')) {
            return abort('403', 'you don\'t have permission to perform this action');
        }
        $phone = $this->users->enable($id);
        $this->message->send([$phone], 'Your account has been activated. Please login to app to explore the world of jewellery.');


        return redirect(route('users.index'));
    }

    /*
     * enable user
     * */

    public function disable($id)
    {
        if (!Auth::user()->hasRole('super-admin')) {
            return abort('403', 'you don\'t have permission to perform this action');
        }
        if (Auth::id() == $id) {
            Session::flash('error', 'You cannot disable yourself');
            return back();
        }
        $this->users->disable($id);

        return redirect(route('users.index'));
    }

    public function details(User $user){
        return view('admin.users.user-details', compact('user'));
    }
}
