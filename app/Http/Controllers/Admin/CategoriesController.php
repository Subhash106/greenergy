<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use App\Repositories\Category\CategoryRepositoryContract;
use Illuminate\Http\Request;
use App\Models\Admin\MainCategory;

class CategoriesController extends Controller
{
    protected $category;

    public function __construct(CategoryRepositoryContract $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->paginate(10);
        //dd($categories);

        return view('admin.categories.index', compact('categories'));
    }

    public function getCategoryByMainCategory(Request $request)
    {
        if (isset($request->main_category_id)) {
            $main_category_id = (int)$request->main_category_id;
            $categories = $this->category->getByMainCategoryId($main_category_id);
            if (count($categories)) {
                $response['status'] = 'success';
                $response['data'] = $categories;
            } else {
                $response['status'] = 'failure';
                $response['error'] = 'No category is available';
            }
        }else{
            $response['status'] = 'failure';
            $response['error'] = 'Main category id not found';
        }

        return response()->json($response);
    }

    public function create()
    {
        $main_categories = MainCategory::select('id', 'name')->get();
        return view('admin.categories.create', compact('main_categories'));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'image_path' => 'required|mimes:jpeg,jpg,png'
        ]);

        $this->category->create($input);

        return redirect(route('categories.index'));
    }

    public function edit($id)
    {
        $main_categories = MainCategory::select('id', 'name')->get();
        $category = $this->category->getById($id);
        return view('admin.categories.edit', compact('category','main_categories'));
    }

    public function update(Request $request, int $id)
    {
        $input = $request->validate([
            'name' => 'required',
            'image_path' => 'required|mimes:jpeg,jpg,png'
        ]);

        $this->category->update($input, $id);

        return redirect(route('categories.index'));
    }

    public function destroy(int $id)
    {
        $this->category->delete($id);

        return redirect(route('categories.index'));
    }
}
