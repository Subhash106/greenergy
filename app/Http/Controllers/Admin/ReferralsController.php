<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Referral\ReferralRepositoryContract;
use Illuminate\Http\Request;

class ReferralsController extends Controller
{
    /**
     * @var ReferralRepositoryContract
     */
    protected $referrals;

    public function __construct(ReferralRepositoryContract $referrals)
    {
        $this->referrals = $referrals;
    }

    /*
     * show all referrals
     * */
    public function index()
    {
        $referrals = $this->referrals->paginate(10);
        return view('admin.referrals.index', compact('referrals'));
    }

    /*
     * create referral
     * */
    public function create()
    {
        return view('admin.referrals.create');
    }

    /*
     * send referral
     * */
    public function store(Request $request)
    {
        $data = $request->validate([
           'phone' => 'required|digits:10',
           'message' => 'required'
        ]);
        $this->referrals->create($data);
        return redirect(route('referrals.index'));
    }
}
