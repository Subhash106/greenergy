<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Banner;
use App\Repositories\Banner\BannerRepositoryContract;
use App\Models\Admin\BannerCategory;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    protected $banner;

    public function __construct(BannerRepositoryContract $banner)
    {
        $this->banner = $banner;
    }

    public function index()
    {
        $banners = $this->banner->paginate(10);

        return view('admin.banners.index', compact('banners'));
    }

    public function create()
    {
        $banner_categories = BannerCategory::all();
        return view('admin.banners.create', compact('banner_categories'));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'caption' => 'required',
            'banner_category_id' => 'required',
            'image_path' => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        $this->banner->create($request->only('caption','banner_category_id','image_path','status'));

        return redirect(route('banners.index'));
    }

    public function edit($id)
    {
        $banner = $this->banner->getById($id);
        $banner_categories = BannerCategory::all();
        return view('admin.banners.edit', compact('banner','banner_categories'));
    }

    public function update(Request $request, int $id)
    {
        $rules = [
            'caption' => 'required',
            'banner_category_id' => 'required'
        ];
        if($request->hasFile('image_path'))
            $rules['image_path'] = 'mimes:jpeg,bmp,png,jpg';
        $input = $request->validate($rules);
        $this->banner->update($input, $id);

        return redirect(route('banners.index'));
    }

    public function changeStatus(Banner $banner)
    {
        $banner->status = !$banner->status;
        $banner->save();
        return back();
    }

    public function destroy(int $id)
    {
        $this->banner->delete($id);

        return redirect(route('banners.index'));
    }
}
