<?php


namespace App\Repositories\SendMessage;

use App\Repositories\SendMessage\SendMessageRepositoryContract;


class SendTextLocalMessageRepository implements SendMessageRepositoryContract
{
    public function send($phones, $message)
    {
        // Account details
        $apiKey = urlencode(env('TEXT_LOCAL_API_KEY', 'OmN81fYM9Gc-E3yMJSUUoqYQEVg30d79SIeHCJQVKb'));

        // Message details
        $sender = urlencode(env('TEXT_LOCAL_SMS_HEADER', 'MBjALT'));

        $message = rawurlencode($message);
        $numbers = implode(',', $phones);
        //dd($message);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        // Process your response here
        return $response;
    }
}
