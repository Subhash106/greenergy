<?php


namespace App\Repositories\SendMessage;

interface SendMessageRepositoryContract
{
    public function send($phones, $message);
}
