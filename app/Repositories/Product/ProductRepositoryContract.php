<?php


namespace App\Repositories\Product;


use Illuminate\Http\Request;

interface ProductRepositoryContract
{
    public function paginate(int $pages);

    public function getById(int $id);

    public function create(array $data);

    public function update(array $data, int $id);

    public function delete(int $id);

    public function publish($id);

    public function unPublish($id);
}
