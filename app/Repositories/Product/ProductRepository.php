<?php


namespace App\Repositories\Product;

use App\Models\Admin\Image;
use App\Models\Admin\Product;
use App\Models\Admin\Thumbnail;
use App\Repositories\Product\ProductRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class ProductRepository implements ProductRepositoryContract
{
    public function paginate(int $pages)
    {
        return Product::paginate($pages);
    }

    public function getById(int $id)
    {
        return Product::find($id);
    }

    public function create(array $data)
    {
        $image = $data['image'];
        $thumbnails = $data['thumbnails'];

        unset($data['image']);
        unset($data['thumbnails']);
        $product = Product::create($data);

        $this->uploadAndSaveImage($product, $image);
        $this->uploadAndSaveThumbnails($product, $thumbnails);
        return $product;
    }

    public function update(array $data, int $id)
    {
        $product = $this->getById($id);
        if (isset($data['image'])) {
            $image = $data['image'];
            unset($data['image']);
            $this->uploadAndSaveImage($product, $image);
        }

        if (isset($data['thumbnails']) && $data['thumbnails'][0] !== null) {
            $thumbnails = $data['thumbnails'];
            unset($data['thumbnails']);
            $this->uploadAndSaveThumbnails($product, $thumbnails);
        }

        $product->name = $data['name'];
        $product->category_id = $data['category_id'];
        $product->price = $data['price'];
        $product->save();

        return $product;
    }

    public function delete(int $id)
    {
        $product = $this->getById($id);
        $product->thumbnails()->delete();
        $product->delete();
    }

    protected function uploadAndSaveImage($product, $image)
    {
//        $path = Storage::disk('s3')->put('dev-product-images', $image);
//        $image_url = 'https://mbj-sub-bucket.s3.ap-south-1.amazonaws.com/'.$path;
        $imageDir = public_path('images');
        if (!is_dir($imageDir)) {
            mkdir($imageDir, 0777);
        }
        $image_url = time().".".$image->extension();
        $image->move($imageDir, $image_url);
        $imageObj = new Image();
//        $imageObj->url = $image_url;
        $imageObj->url = asset('images/' . $image_url);
        $product->image()->save($imageObj);
    }

    protected function uploadAndSaveThumbnails($product, $thumbnails){
        $thumbnailDir = public_path('thumbnails');
        if (!is_dir($thumbnailDir)) {
            mkdir($thumbnailDir, 0777);
        }

        foreach ($thumbnails as $thumbnail) {
//            $path = Storage::disk('s3')->put('dev-product-images', $thumbnail);
//            $thumbnailName = 'https://mbj-sub-bucket.s3.ap-south-1.amazonaws.com/'.$path;
            $thumbnailName = time().".".$thumbnail->extension();
            $thumbnail->move($thumbnailDir, $thumbnailName);

            $thumbnailObj = new Thumbnail;
//            $thumbnailObj->url = $thumbnailName;
            $thumbnailObj->url = asset('thumbnails/' . $thumbnailName);
            $product->thumbnails()->save($thumbnailObj);
        }
    }

    public function publish($id)
    {
        $product = $this->getById($id);
        $product->publish();
    }

    public function unPublish($id)
    {
        $product = $this->getById($id);

        $product->unPublish();
    }
}
