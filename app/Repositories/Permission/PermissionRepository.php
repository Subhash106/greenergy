<?php


namespace App\Repositories\Permission;

use App\Models\Admin\Permission;
use App\Repositories\Permission\PermissionRepositoryContract;
use Illuminate\Http\Request;

class PermissionRepository implements PermissionRepositoryContract
{
    public function getAll()
    {
        return Permission::all();
    }

    public function paginate(int $pages)
    {
        return Permission::paginate($pages);
    }

    public function getById(int $id)
    {
        return Permission::find($id);
    }

    public function create(array $data)
    {
        return Permission::create($data);
    }

    public function update(array $data, int $id)
    {
        $permission = $this->getById($id);
        $permission->permission = $data['permission'];
        $permission->slug = $data['slug'];
        $permission->description = $data['description'];
        $permission->save();

        return $permission;
    }

    public function delete(int $id)
    {
        $permission = $this->getById($id);
        $permission->delete();
    }
}
