<?php


namespace App\Repositories\Category;


use Illuminate\Http\Request;

interface CategoryRepositoryContract
{
    public function getAll();

    public function paginate(int $pages);

    public function getById(int $id);

    public function getByMainCategoryId(int $id);

    public function create(array $data);

    public function update(array $data, int $id);

    public function delete(int $id);
}
