<?php


namespace App\Repositories\Category;

use App\Models\Admin\Category;
use App\Models\Admin\MainCategory;
use App\Repositories\Category\CategoryRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class CategoryRepository implements CategoryRepositoryContract
{
    public function getAll()
    {
        return Category::all();
    }

    public function paginate(int $pages)
    {
        return Category::paginate($pages);
    }

    public function getById(int $id)
    {
        return Category::find($id);
    }

    public function getByMainCategoryId(int $id)
    {
        $main_category = MainCategory::find($id);
        $categories = $main_category->categories;
        return $categories;
    }

    public function create(array $data)
    {
        $image = $data['image_path'];
        unset($data['image_path']);
        $data['image_path'] = $this->uploadAndSaveImage($image);

        return Category::create($data);
    }

    public function update(array $data, int $id)
    {
        if(isset($data['image_path']))
        {
            $image = $data['image_path'];
            unset($data['image_path']);
            $data['image_path'] = $this->uploadAndSaveImage($image);
        }
        $category = $this->getById($id);
        $category->update($data);

        return $category;
    }

    public function delete(int $id)
    {
        $category = $this->getById($id);
        $category->delete();
    }

    protected function uploadAndSaveImage($image)
    {
        $category_image = time().".".$image->getClientOriginalExtension();
        $image->move(public_path('category_images'), $category_image);
//        $path = Storage::disk('s3')->put('category-images', $image);
//        return 'https://mbj-sub-bucket.s3.ap-south-1.amazonaws.com/'.$path;
        return asset('category_images/'.$category_image);
    }
}
