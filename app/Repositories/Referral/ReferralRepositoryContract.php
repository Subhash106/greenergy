<?php


namespace App\Repositories\Referral;


use Illuminate\Http\Request;

interface ReferralRepositoryContract
{
    public function getAll();

    public function paginate(int $pages);

    public function getById(int $id);

    public function create(array $data);
}
