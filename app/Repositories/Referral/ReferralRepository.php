<?php


namespace App\Repositories\Referral;

use App\Models\Admin\Referral;
use App\Repositories\Referral\ReferralRepositoryContract;
use App\Repositories\SendMessage\SendMessageRepositoryContract;
use Illuminate\Support\Facades\Log;

class ReferralRepository implements ReferralRepositoryContract
{
    /**
     * @var SendMessageRepositoryContract
     */
    protected $messageService;

    public function __construct(SendMessageRepositoryContract $messageService)
    {
        $this->messageService = $messageService;
    }

    public function getAll()
    {
        return Referral::all();
    }

    public function paginate(int $pages)
    {
        return Referral::paginate($pages);
    }

    public function getById(int $id)
    {
        return Referral::find($id);
    }

    public function create(array $data)
    {
        //send message
        $massage = "Please install MBJ app by clicking appLinkHere";
        $response = $this->messageService->send(array($data['phone']), $massage);
        Log::info('send message response: '. $response);
        return Referral::create($data);
    }
}
