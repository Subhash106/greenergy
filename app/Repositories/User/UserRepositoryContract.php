<?php


namespace App\Repositories\User;


use Illuminate\Http\Request;

interface UserRepositoryContract
{
    public function getAll();

    public function paginate(int $pages);

    public function getById(int $id);

    public function create(array $data);

    public function update(array $data, int $id);

    public function delete(int $id);

    public function enable(int $id);

    public function disable(int $id);
}
