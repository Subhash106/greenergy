<?php


namespace App\Repositories\User;

use App\User;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Http\Request;

class UserRepository implements UserRepositoryContract
{
    public function getAll()
    {
        return User::all();
    }

    public function paginate(int $pages)
    {
        return User::paginate($pages);
    }

    public function getById(int $id)
    {
        return User::find($id);
    }

    public function create(array $data)
    {
        return User::create($data);
    }

    public function update(array $data, int $id)
    {
        $role = $this->getById($id);
        $role->role = $data['role'];
        $role->slug = $data['slug'];
        $role->description = $data['description'];
        $role->save();

        return $role;
    }

    public function delete(int $id)
    {
        $role = $this->getById($id);
        $role->delete();
    }
    /*
     * enable user
     * */
    public function enable($id){
        $phone =  $this->getById($id)->enable();
        return $phone;
    }

    /*
     * disable user
     * */
    public function disable($id){
        return $this->getById($id)->disable();
    }
}
