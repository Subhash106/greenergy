<?php


namespace App\Repositories\Banner;

use App\Models\Admin\Banner;
use App\Repositories\Banner\BannerRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class BannerRepository implements BannerRepositoryContract
{
    public function getAll()
    {
        return Banner::all();
    }

    public function paginate(int $pages)
    {
        return Banner::paginate($pages);
    }

    public function getById(int $id)
    {
        return Banner::find($id);
    }

    public function create(array $data)
    {
        $image = $data['image_path'];
        unset($data['image_path']);
        $data['image_path'] = $this->uploadAndSaveImage($image);
        return Banner::create($data);
    }

    protected function uploadAndSaveImage($image)
    {
        $path = Storage::disk('s3')->put('banner-images', $image);
        return 'https://mbj-sub-bucket.s3.ap-south-1.amazonaws.com/'.$path;
    }

    public function update(array $data, int $id)
    {
        if(isset($data['image_path']))
        {
            $image = $data['image_path'];
            unset($data['image_path']);
            $data['image_path'] = $this->uploadAndSaveImage($image);
        }
        $banner = $this->getById($id);
        $banner->update($data);
        return $banner;
    }

    public function delete(int $id)
    {
        $banner = $this->getById($id);
        $banner->delete();
    }
}
