<?php


namespace App\Repositories\Banner;


use Illuminate\Http\Request;

interface BannerRepositoryContract
{
    public function getAll();

    public function paginate(int $pages);

    public function getById(int $id);

    public function create(array $data);

    public function update(array $data, int $id);

    public function delete(int $id);
}
