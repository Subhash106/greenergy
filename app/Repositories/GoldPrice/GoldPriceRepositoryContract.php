<?php


namespace App\Repositories\GoldPrice;


use Illuminate\Http\Request;

interface GoldPriceRepositoryContract
{
    public function getAll();

    public function paginate(int $pages);

    public function getById(int $id);

    public function create(array $data);

}
