<?php


namespace App\Repositories\GoldPrice;

use App\Models\Admin\GoldPrice;
use App\Repositories\GoldPrice\GoldPriceRepositoryContract;
use Illuminate\Http\Request;

class GoldPriceRepository implements GoldPriceRepositoryContract
{
    public function getAll()
    {
        return GoldPrice::all();
    }

    public function paginate(int $pages)
    {
        return GoldPrice::paginate($pages);
    }

    public function getById(int $id)
    {
        return GoldPrice::find($id);
    }

    public function create(array $data)
    {
        $price = GoldPrice::where('state_id',$data['state_id'])->whereDate('created_at' ,date("Y-m-d", time()))->first();
        if($price)
            return $price->update($data);
        return GoldPrice::create($data);
    }

}
