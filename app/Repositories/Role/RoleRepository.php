<?php


namespace App\Repositories\Role;

use App\Models\Admin\Role;
use App\Repositories\Role\RoleRepositoryContract;
use Illuminate\Http\Request;

class RoleRepository implements RoleRepositoryContract
{
    public function getAll()
    {
        return Role::all();
    }

    public function paginate(int $pages)
    {
        return Role::paginate($pages);
    }

    public function getById(int $id)
    {
        return Role::find($id);
    }

    public function create(array $data)
    {
        return Role::create($data);
    }

    public function update(array $data, int $id)
    {
        $role = $this->getById($id);
        $role->role = $data['role'];
        $role->slug = $data['slug'];
        $role->description = $data['description'];
        $role->save();

        return $role;
    }

    public function delete(int $id)
    {
        $role = $this->getById($id);
        $role->delete();
    }
}
