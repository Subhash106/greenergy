<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Role\RoleRepositoryContract', 'App\Repositories\Role\RoleRepository'
        );

        $this->app->bind(
            'App\Repositories\Permission\PermissionRepositoryContract', 'App\Repositories\Permission\PermissionRepository'
        );

        $this->app->bind(
            'App\Repositories\Category\CategoryRepositoryContract', 'App\Repositories\Category\CategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Product\ProductRepositoryContract', 'App\Repositories\Product\ProductRepository'
        );

        $this->app->bind(
            'App\Repositories\Referral\ReferralRepositoryContract', 'App\Repositories\Referral\ReferralRepository'
        );

        $this->app->bind(
            'App\Repositories\SendMessage\SendMessageRepositoryContract', 'App\Repositories\SendMessage\SendTextLocalMessageRepository'
        );

        $this->app->bind(
            'App\Repositories\User\UserRepositoryContract', 'App\Repositories\User\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\GoldPrice\GoldPriceRepositoryContract', 'App\Repositories\GoldPrice\GoldPriceRepository'
        );
        $this->app->bind(
            'App\Repositories\Banner\BannerRepositoryContract', 'App\Repositories\Banner\BannerRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
