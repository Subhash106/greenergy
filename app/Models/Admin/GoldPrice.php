<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GoldPrice extends Model
{
    protected $fillable = ['price', 'state_id'];

    public function state()
    {
        return $this->hasOne('\App\Models\Admin\State', 'id', 'state_id');
    }
}
