<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [ 'permission', 'slug', 'description' ];

    public function roles() {
        return $this->belongsToMany( '\App\Models\Admin\Role',
            'permission_role', 'permission_id', 'role_id'
        )->withPivot('id');
    }
}
