<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Admin\Product;

class ProductCallRequest extends Model
{
    protected $fillable = ['user_id', 'product_id'];

    public function user()
    {
        return $this->hasOne(User::class, 'id' ,'user_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id' ,'product_id');
    }
}
