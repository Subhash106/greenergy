<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Product;

class Package extends Model
{
    public function products()
    {
        return $this->belongsToMany('Product');
    }
}
