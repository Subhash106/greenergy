<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [ 'role', 'slug', 'description' ];

    public function permissions() {
        return $this->belongsToMany( '\App\Models\Admin\Permission',
            'permission_role', 'role_id', 'permission_id'
        )->withPivot('id');
    }

    public function users() {
        return $this->belongsToMany( '\App\Models\Admin\Permission',
            'permission_role', 'permission_id', 'user_id'
        )->withPivot('id');
    }
}
