<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $fillable = ['product_id', 'user_id'];
    protected $table = 'wishlists';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
