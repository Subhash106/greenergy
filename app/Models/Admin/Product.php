<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Package;

class Product extends Model
{
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function mainCategory()
    {
        return $this->belongsTo(MainCategory::class);
    }

    public function thumbnails(){
        return $this->hasMany(Thumbnail::class);
    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getPublishedAt($value){
        return $value;
    }

    public function publish(){
        $this->published_at = date('Y-m-d H:i:s');
        $this->save();
    }

    public function unPublish(){
        $this->published_at = NULL;
        $this->save();
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class);
    }
}
