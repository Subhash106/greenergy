<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $guarded = [];
}
