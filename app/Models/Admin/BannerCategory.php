<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Banner;

class BannerCategory extends Model
{
    public $timestamps = false;

    public function banners()
    {
        return $this->hasMany(Banner::class, 'banner_category_id','id');
    }

}
