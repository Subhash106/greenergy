<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Admin\BannerCategory;

class Banner extends Model
{
    protected $guarded = [];

    public function bannerCategory()
    {
        return $this->hasOne('\App\Models\Admin\BannerCategory','id', 'banner_category_id');
    }
}
