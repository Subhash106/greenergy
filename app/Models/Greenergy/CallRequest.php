<?php

namespace App\Models\Greenergy;

use Illuminate\Database\Eloquent\Model;

class CallRequest extends Model
{
    protected $table = 'call_requests';
    protected $guarded = [];
}
