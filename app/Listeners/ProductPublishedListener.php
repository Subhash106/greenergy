<?php

namespace App\Listeners;

use App\Events\ProductPublished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProductPublishedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductPublished  $event
     * @return void
     */
    public function handle(ProductPublished $event)
    {
        var_dump('Product is published');
    }
}
