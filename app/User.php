<?php

namespace App;

use App\Models\Admin\Product;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'status', 'anniversary_date', 'dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roles() {
        return $this->belongsToMany( '\App\Models\Admin\Role',
            'role_user', 'user_id', 'role_id'
        );
    }

    public function permissions()
    {
        $roles = $this->roles()->get();
        $permissions_list = array();
        foreach ($roles as $role) {
            $permissions = $role->permissions()->get();
            foreach ($permissions as $permission) {
                $permissions_list[$permission->id] = $permission->toArray();
            }
        }
        return $permissions_list;
    }

    public function hasRole($role_slug)
    {
        foreach ($this->roles()->get() as $role) {
            if ($role->slug == $role_slug)
            {
                return true;
            }
        }
        return false;
    }

    public function hasPermission($permission_slug)
    {

        $permission_slug = Str::slug($permission_slug);

        foreach ($this->permissions() as $permission) {
            if ($permission['slug'] == $permission_slug) {
                return true;
            }
        }
        return false;
    }

    public function enable(){
        $this->status = 1;
        $this->save();
        return $this->phone;
    }

    public function disable(){
        $this->status = 0;
        $this->save();
    }

    public function productsForCallback()
    {
        return $this->belongsToMany(Product::class, 'product_call_requests', 'user_id', 'product_id');
    }
}
