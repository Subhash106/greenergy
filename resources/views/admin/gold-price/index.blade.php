@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Gold Price</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" href="{{ route('gold-price.create') }}">
                            {{ __('Add Today\'s Price') }}
                        </a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Price <small>(per 10g)</small></th>
                                <th>State</th>
                                <th>Created_at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($gold_prices as $key=>$price)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$price->price}}</td>
                                    <td>{{ucwords(strtolower($price->state->state))}}</td>
                                    <td>{{$price->updated_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$gold_prices->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
