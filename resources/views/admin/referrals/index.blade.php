@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Referrals</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" href="{{ route('referrals.create') }}">
                            {{ __('Send Referral') }}
                        </a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Phone</th>
                                <th>Message</th>
                                <th>Send At</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach ($referrals as $key => $referral)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $referral->phone }}</a></td>
                                    <td>{{ $referral->message }}</td>
                                    <td>{{ date('d-M-Y H:i:s') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$referrals->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
