<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
    <div class="col-md-6">
        <input id="phone" maxlength="10" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone"
               value="{{ isset($referral->phone) ? $referral->phone : old('phone') }}" required autocomplete="phone">

        @error('phone')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Message') }}</label>

    <div class="col-md-6">
        <textarea id="message" type="text" class="form-control @error('message') is-invalid @enderror" name="message"
                  required autocomplete="message">{{ isset($referral->message) ? $referral->message : old('message') }}</textarea>
        @error('message')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
