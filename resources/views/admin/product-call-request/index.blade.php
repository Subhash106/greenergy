@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Call Requests</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Call Requests List</strong>
                    </div>

                    <div class="card-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>User Name</th>
                                <th>Product Name</th>
                                <th>Requested At</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($call_requests as $key => $call_request)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><a href="{{route('users.details', $call_request->user->id)}}">{{$call_request->user->name}}</a></td>
                                    <td><a href="{{route('products.show', $call_request->product->id)}}">{{$call_request->product->name}}</a></td>
                                    <td>{{$call_request->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$call_requests->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
