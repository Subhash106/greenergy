<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
    <div class="col-md-6">
        <input id="role" type="text" class="form-control @error('role') is-invalid @enderror" name="role"
               value="{{ isset($role->role) ? $role->role : old('role') }}" required autocomplete="role">

        @error('role')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

    <div class="col-md-6">
        <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description"
                  required autocomplete="description">{{ isset($role->description) ? $role->description : old('description') }}</textarea>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
