@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Users') }}
                    </div>

                    <div class="card-body">
                        <table class="table table-borderless" >
                            <tr> <th>Name</th><td>{{$user->name}}</td> </tr>
                            <tr> <th>Phone</th><td>{{ $user->phone }}</td></tr>
                            <tr> <th>Email</th><td>{{ $user->email }}</td></tr>
                            <tr> <th>Anniversary Date</th><td>{{ $user->anniversary_date }}</td></tr>
                            <tr> <th>Date of Birth</th><td>{{ $user->dob }}</td> </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
