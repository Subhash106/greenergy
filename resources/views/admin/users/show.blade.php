@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active" aria-current="page">Role Details</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Role Details') }}
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-sm-2" for="email">Role:</label>
                                    <div class="col-sm-10">
                                        <strong>{{ $role->role }}</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-sm-2" for="email">Slug:</label>
                                    <div class="col-sm-10">
                                        <strong>{{ $role->slug }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label" for="email">Description:</label>
                                <p>
                                    <strong>@if(is_null($role->description)) Not Provided @else {{$role->description}} @endif</strong>
                                </p>
                            </div>
                        </div>
                        @if(count($assigned_permissions))
                            <hr>
                            <h4>Assigned Permissions</h4>
                            <div id="permissions" style="margin: 15px 3px 15px 0;">
                                @foreach($assigned_permissions as $permission)
                                    <div style="background-color: pink;padding: 3px 8px;font-weight: bold; display: inline-block">
                                        <form action="{{route('remove-permission', $permission->pivot->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <span>{{$permission->permission}}</span>
                                            <input type="hidden" value="{{$permission->id}}">
                                            <input type="submit" value="X" name="delete" class="btn btn-sm btn-danger" style="padding: 0 4px">
                                        </form>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <h4>Assign Permissions</h4>
                        <form action="{{route('assign-permissions', $role->id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <select class="form-control" id="sel1" name="permissions[]" multiple>
                                    <option>Select Permissions</option>
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->id}}">{{$permission->permission}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" name="submit" class="btn btn-info btn-sm" value="Assign">
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
