@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Users') }}
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Created At</th>
                                <th>Status</th>
                                <th>Change Status</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><a href="#">{{$user->name}}</a></td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ implode(', ', $user->roles->pluck('role')->toArray()) }}</td>
                                    <td>{{ date('d-m-Y H:i:s', strtotime($user->created_at)) }}</td>
                                    <td>{{ $user->status ? 'Active' : 'InActive' }}</td>
                                    <td>
                                        @if($user->status)
                                        <form method="POST" action="{{route('users.disable', $user->id)}}">
                                            @csrf
                                            @method('PATCH')
                                            <button class="btn btn-danger">
                                                Disable
                                            </button>
                                        </form>
                                        @else
                                            <form method="POST" action="{{route('users.enable', $user->id)}}">
                                                @csrf
                                                @method('PATCH')
                                                <button class="btn btn-success">
                                                    Enable
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$users->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
