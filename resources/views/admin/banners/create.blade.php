@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('banners.index')}}">Banners</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Banner</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Add Banner') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('banners.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="banner_category_id" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                                <div class="col-md-6">
                                    <select name="banner_category_id" class="form-control @error('banner_category_id') is-invalid @enderror">
                                        <option value="">Select Banner Category</option>
                                        @foreach($banner_categories as $category)
                                            <option value="{{$category->id}}" @if($category->id == old('banner_category_id')) selected @endif>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('banner_category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="caption" class="col-md-4 col-form-label text-md-right">{{ __('Caption') }}</label>

                                <div class="col-md-6">
                                    <textarea id="caption" type="text" class="form-control @error('caption') is-invalid @enderror"
                                           name="caption" autocomplete="caption" autofocus>{{ old('caption') }}</textarea>
                                    @error('caption')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image_path" class="col-md-4 col-form-label text-md-right">{{ __('Banner Image') }}</label>

                                <div class="col-md-6">
                                    <input id="image_path" type="file" class="form-control @error('image_path') is-invalid @enderror"
                                           name="image_path" value="{{ old('image_path') }}" autocomplete="image_path" autofocus>

                                    @error('image_path')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-6">
                                    <input type="radio" name="status" checked value="1" checked> Enable <br> <input type="radio" name="status" value="0"> Disable

                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>



                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
