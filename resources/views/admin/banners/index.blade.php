@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Banners</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" href="{{ route('banners.create') }}">
                            {{ __('Add Banner') }}
                        </a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Image</th>
                                <th>Banner Category</th>
                                <th>Caption</th>
                                <th>status</th>
                                <th>Change Status</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($banners as $key => $banner)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img src="{{$banner->image_path}}" alt="{{$banner->name}}" width="auto" height="60"></td>
                                    <td>{{$banner->bannerCategory->name}}</td>
                                    <td>{{$banner->caption}}</td>
                                    <td>
                                        @if($banner->status) Enabled @else Disabled @endif
                                    </td>
                                    <td>
                                        <form action="{{route('banner-status-change', $banner->id)}}" method="post">
                                            @method('put')
                                            @csrf
                                            <input type="submit" name="update" class="btn btn-warning" value="@if($banner->status) Disable @else Enable @endif">
                                        </form>
                                    </td>
                                    <td>
                                        <a href="{{route('banners.edit', $banner->id)}}" class="btn btn-success">
                                            Edit
                                        </a>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{route('banners.destroy', $banner->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$banners->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
