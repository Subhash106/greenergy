@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" href="{{ route('products.create') }}">
                            {{ __('Create New Product') }}
                        </a>
                        <a class="btn btn-primary" href="{{ route('products.bulk-upload') }}">
                            {{ __('Upload Product In Bulk') }}
                        </a>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($products as $key => $product)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><a href="{{route('products.show', $product->id)}}">{{$product->name}}</a></td>
                                    <td><img src="@if(isset($product->image->url)) {{ $product->image->url }} @else {{ asset('default-image.jpg') }} @endif" alt="{{$product->name}}" height="50px" width="auto"></td>
                                    <td>{{ $product->price }}</td>
                                    <td>
                                        <a href="{{route('products.edit', $product->id)}}" class="btn btn-success">
                                            Edit
                                        </a>
                                    </td>

                                    <td>
                                        <form method="POST" action="{{route('products.destroy', $product->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">
                                                Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$products->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
