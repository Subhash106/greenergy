<div class="form-group row">
    <label for="category_id" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

    <div class="col-md-6">
        <select id="category_id" class="form-control @error('category_id') is-invalid @enderror" name="category_id"
                value="{{ old('category_id') }}" required autocomplete="category_id" autofocus>
            <option value="">Select Category</option>
            @foreach( $categories as $category)
            <option value="{{ $category->id }}"
                    @if(isset($product->category_id)) @if($category->id == $product->category_id) selected @endif @endif>
                {{ $category->name }}
            </option>
            @endforeach
        </select>

        @error('category_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
               value="{{ isset($product->name) ? $product->name : old('name') }}" required autocomplete="name">

        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

    <div class="col-md-6">
        <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price"
               value="{{ isset($product->price) ? $product->price : old('price') }}" required autocomplete="price">

        @error('price')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Main Image') }}</label>

    <div class="col-md-6">
        <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image">

        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        @if(isset($product->image) && !is_null($product->image))
            <img style="width: 100px;height: 100px;" src="{{ $product->image->url }}">
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="thumbnails" class="col-md-4 col-form-label text-md-right">{{ __('Thumbnails') }}</label>

    <div class="col-md-6">
        <input id="thumbnails"
               type="file"
               class="form-control @error('thumbnails') is-invalid @enderror"
               name="thumbnails[]"
               multiple="multiple">

        @error('thumbnails')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        @if(isset($product->thumbnails) && !is_null($product->thumbnails))
            @foreach($product->thumbnails as $thumbnail)
                <img style="width: 50px;height: 50px;" src="{{ $thumbnail->url }}">
            @endforeach
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Ingredients') }}</label>

    <div class="col-md-6">
        <textarea id="ingredients" type="text" class="form-control @error('ingredients') is-invalid @enderror" name="ingredients"
                  required autocomplete="ingredients">{{ isset($product->ingredients) ? $product->ingredients : old('ingredients') }}</textarea>

        @error('ingredients')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="energy" class="col-md-4 col-form-label text-md-right">{{ __('Energy (KJ)') }}</label>

    <div class="col-md-6">
        <input id="energy" type="text" class="form-control @error('energy') is-invalid @enderror" name="energy"
               value="{{ isset($product->energy) ? $product->energy : old('energy') }}" required autocomplete="energy">

        @error('energy')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="protein" class="col-md-4 col-form-label text-md-right">{{ __('Protein') }}</label>

    <div class="col-md-6">
        <input id="protein" type="text" class="form-control @error('protein') is-invalid @enderror" name="protein"
               value="{{ isset($product->protein) ? $product->protein : old('protein') }}" required autocomplete="protein">

        @error('protein')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="dietary_fiber" class="col-md-4 col-form-label text-md-right">{{ __('Dietary Fiber') }}</label>

    <div class="col-md-6">
        <input id="dietary_fiber" type="text" class="form-control @error('dietary_fiber') is-invalid @enderror" name="dietary_fiber"
               value="{{ isset($product->dietary_fiber) ? $product->dietary_fiber : old('dietary_fiber') }}" required autocomplete="dietary_fiber">

        @error('dietary_fiber')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="carbohydrates" class="col-md-4 col-form-label text-md-right">{{ __('Carbohydrates') }}</label>

    <div class="col-md-6">
        <input id="carbohydrates" type="text" class="form-control @error('carbohydrates') is-invalid @enderror" name="carbohydrates"
               value="{{ isset($product->carbohydrates) ? $product->carbohydrates : old('carbohydrates') }}" required autocomplete="carbohydrates">

        @error('carbohydrates')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="sugar" class="col-md-4 col-form-label text-md-right">{{ __('Sugar') }}</label>

    <div class="col-md-6">
        <input id="sugar" type="text" class="form-control @error('sugar') is-invalid @enderror" name="sugar"
               value="{{ isset($product->sugar) ? $product->sugar : old('sugar') }}" required autocomplete="sugar">

        @error('sugar')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>


<div class="form-group row">
    <label for="total_fat" class="col-md-4 col-form-label text-md-right">{{ __('Total Fat') }}</label>

    <div class="col-md-6">
        <input id="total_fat" type="text" class="form-control @error('total_fat') is-invalid @enderror" name="total_fat"
               value="{{ isset($product->total_fat) ? $product->total_fat : old('total_fat') }}" required autocomplete="total_fat">

        @error('total_fat')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="saturated_fat" class="col-md-4 col-form-label text-md-right">{{ __('Saturated Fat') }}</label>

    <div class="col-md-6">
        <input id="saturated_fat" type="text" class="form-control @error('saturated_fat') is-invalid @enderror" name="saturated_fat"
               value="{{ isset($product->saturated_fat) ? $product->saturated_fat : old('saturated_fat') }}" required autocomplete="saturated_fat">

        @error('saturated_fat')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="fiber" class="col-md-4 col-form-label text-md-right">{{ __('Fiber') }}</label>

    <div class="col-md-6">
        <input id="fiber" type="text" class="form-control @error('fiber') is-invalid @enderror" name="fiber"
               value="{{ isset($product->fiber) ? $product->fiber : old('fiber') }}" required autocomplete="fiber">

        @error('fiber')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="calories" class="col-md-4 col-form-label text-md-right">{{ __('Calories') }}</label>

    <div class="col-md-6">
        <input id="calories" type="text" class="form-control @error('calories') is-invalid @enderror" name="calories"
               value="{{ isset($product->calories) ? $product->calories : old('calories') }}" required autocomplete="calories">

        @error('calories')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
