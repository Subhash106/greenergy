@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('products.index')}}">Products</a></li>
                <li class="breadcrumb-item active" aria-current="page">Product Details</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Product Details') }}
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <span class="h4">Product Title: {{ $product->name }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <img style="width: 100%;" src="@if(isset($product->image->url)) {{ $product->image->url }}
                                @else {{ asset('default-image.jpg') }} @endif">
                                <br><br>
                                <div class="row">
                                    @foreach($product->thumbnails as $thumbnail)
                                        <div class="col-md-4">
                                            <img class="img-thumbnail rounded" src="@if(isset($thumbnail->url)) {{ $thumbnail->url }}
                                            @else {{ asset('default-image.jpg') }} @endif" style="width: 100%; height: 100px">
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-md-6">
                                <table style="width: 100%" id="product-details">
                                    <tr> <th>Ingredients</th> <td>{{$product->ingredients}}</td> </tr>
                                    <tr> <th>Category</th> <td>{{$product->category->name}}</td> </tr>
                                    <tr> <th>Price</th> <td>{{$product->price}} Rs.</td> </tr>
                                    <tr> <th>Protein</th> <td>{{$product->protein}} Rs.</td> </tr>
                                    <tr> <th>Dietary Fiber</th> <td>{{$product->dietary_fiber}} Rs.</td> </tr>
                                    <tr> <th>Carbohydrates</th> <td>{{$product->carbohydrates}} Rs.</td> </tr>
                                    <tr> <th>Sugar</th> <td>{{$product->sugar}} Rs.</td> </tr>
                                    <tr> <th>Total Fat</th> <td>{{$product->total_fat}} Rs.</td> </tr>
                                    <tr> <th>Saturated Fat</th> <td>{{$product->saturated_fat}} Rs.</td> </tr>
                                    <tr> <th>Fiber</th> <td>{{$product->fiber}} Rs.</td> </tr>
                                </table>
                                <hr>
                                @if($product->packages->count())
                                    <h3>Available Packages for {{$product->name}}</h3>
                                    <table width="100%">
                                        @foreach($product->packages as $package)
                                            <tr>
                                                <td>{{$package->units." ".$package->measuring_unit}}</td>
                                                <td>
                                                    <form action="{{route('remove-package', $product->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="package_id" value="{{ $package->id }}">
                                                        <input type="submit" value="X" class="btn btn-danger btn-sm">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif
                                <h3>Add Packages</h3>
                                <form action="{{ route('add-packages', $product->id) }}" method="post">
                                    @csrf
                                    @foreach($packages as $package)
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" name="packages[]" class="form-check-input"
                                                       value="{{ $package->id }}">
                                                {{ $package->units." ".$package->measuring_unit }}
                                            </label>
                                        </div>
                                    @endforeach
                                    <input type="submit" value="Add Package" class="btn btn-primary">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
