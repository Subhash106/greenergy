@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('products.index')}}">Products</a></li>
                <li class="breadcrumb-item active" aria-current="page">Bulk Upload</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <span>{{ __('Bulk Product Upload') }}</span>
                        <a href="{{route('download-excel-template')}}" class="btn btn-info btn-sm float-right">Download Excel Template</a>

                    </div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('products.store-bulk-upload') }}">
                            @csrf

                            <div class="form-group row mb-0">
                                <div class="col-md-4">
                                    Upload <input type="file" name="products">
                                </div>
                                <div class="col-md-4">
                                    Upload excel file with products details
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
