@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permissions</a></li>
                <li class="breadcrumb-item active" aria-current="page">Permission Details</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Permission Details') }}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-sm-3" for="email">Permission:</label>
                                    <div class="col-sm-9">
                                        <strong>{{ $permission->permission }}</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="control-label col-sm-3" for="email">Slug:</label>
                                    <div class="col-sm-9">
                                        <strong>{{ $permission->slug }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <label class="control-label" for="email">Description:</label>
                                <p>
                                    <strong>@if(is_null($permission->description)) Not Provided @else {{$permission->description}} @endif</strong>
                                </p>
                            </div>
                        </div>
                        @if(count($assigned_roles))
                            <hr>
                            <h4>Assigned Permissions</h4>
                            <div id="roles" style="margin: 15px 3px 15px 0;">
                                @foreach($assigned_roles as $role)
                                    <div style="background-color: pink;padding: 3px 8px;font-weight: bold; display: inline-block">
                                        <form action="{{route('remove-from-role', $role->pivot->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <span>{{$role->role}}</span>
                                            <input type="hidden" value="{{$role->id}}">
                                            <input type="submit" value="X" name="delete" class="btn btn-sm btn-danger" style="padding: 0 4px">
                                        </form>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <h4>Assign this permission to roles</h4>
                        <form action="{{route('assign-to-roles', $permission->id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <select class="form-control" id="sel1" name="roles[]" multiple>
                                    <option>Select Roles</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->role}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" name="submit" class="btn btn-info btn-sm" value="Assign">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
