<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Permission') }}</label>
    <div class="col-md-6">
        <input id="permission" type="text" class="form-control @error('permission') is-invalid @enderror" name="permission"
               value="{{ isset($permission->permission) ? $permission->permission : old('permission') }}" required autocomplete="permission">

        @error('permission')
        <span class="invalid-feedback" permission="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

    <div class="col-md-6">
        <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description"
                  autocomplete="description">{{ isset($permission->description) ? $permission->description : old('description') }}</textarea>
        @error('description')
        <span class="invalid-feedback" permission="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
