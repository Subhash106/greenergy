<!DOCTYPE html>
<html lang="en">
<head>
    <title>Greenergy-FAQ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('greenergy-new-theme/css/style-faq.css')}}">
</head>
<body>
<div class="layout">
    <!--header start-->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="back-arrow">
                        <a href="{{url('/')}}"><img src="{{asset('greenergy-new-theme/images/arrow.png')}}"></a>
                    </div>
                    <div class="logo">
                        <img src="{{asset('greenergy-new-theme/images/logo-faq.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--body container start-->


    <section class="faq-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="left-content">
                        <h2>Faqs :</h2>
                        <h3>When will the delivery start?</h3>
                        <p>The first batch of delivery will begin from 15th July, 2020. </p>

                        <h3>What all you are providing?</h3>
                        <p>The subscription box is designed keeping the nutrition needs of a family in mind. It contains An assortment of vegetables, pulses sourced from our own certified  farms and other health products like immunity boosters that cater to different age groups. The ingredient will also depend on the produce of the season also.</p>
                        <ul>
                            <li>Organic Wheat Flour Loose</li>
                            <li>Organic Corn Flour Loose</li>
                            <li>Chana Daal</li>
                            <li>Organic Besan</li>
                            <li>Arhar Daal</li>
                            <li>Masoor Daal</li>
                            <li>Moong Daal Dhoba</li>
                            <li>Urad Daal Dhoba</li>
                            <li>Organic Honey</li>
                            <li>Triple Refined Sugar</li>
                            <li>Sabut Dhaniya</li>
                            <li>Dhaniya Powder</li>
                            <li>Saunf</li>
                            <li>Haldi Powder</li>
                            <li>Organic Jaggery</li>
                            <li>Organic Sugar</li>
                            <li>Organic Sugar</li>
                            <li>Rusi Rice</li>
                            <li>Sarvati Rice</li>
                            <li>Sama Rice</li>
                            <li>Yellow Mustard Oil</li>
                            <li>Til Oil</li>
                            <li>Sunflower Oil</li>
                            <li>Bajra</li>
                            <li>Juar</li>
                            <li>Organic Chana</li>
                        </ul>
                        <h3>What is your delivery process?</h3>
                        <p>We use efficient logistics to ensure all the nutrition and freshness is retained. We keep a track on weather and we organise our delivery process to be ready to serve you fresh fruits and vegetables. </p>
                        <p>Our supply chain incorporates 6 nodal points across Delhi NCR from where we make quick delivery to cater to the consumer’s needs. </p>
                        <p>Our supply chain incorporates 6 nodal points across Delhi NCR from where we make quick delivery to cater to the consumer’s needs.</p>

                        <h3>Do you have a customer service centre?</h3>
                        <p>It’s a work in progress and will be available soon to cater to the needs of members.</p>

                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="right-content">
                        <h3>How does the subscriptions work?</h3>
                        <p>The subscription model is a simple model that requires an initial deposit of Rs.20000 that not only  ensures membership but also brings in the benefit of  three month home delivery of vegetables, fruits and pulses worth Rs.7000. Fourth month onwards minimum commitment to continue receiving the products is Rs.5000 per month.</p>
                        <div class="how-can-pay bg-box">
                            <h3>How can I pay?</h3>
                            <p>Online Payment accepted at:</p>
                            <h4>Praarabdh Ecomm Private Limited</h4>
                            <p><strong>ICICI BANK</strong></p>
                            <p><strong>A/c No :</strong> 071605002978</p>
                            <p><strong>IFSC : </strong> ICIC0000716</p>
                            <p class="bottom-cntnt">We always want to better your experience. That's why we are working towards more payment options. We will come back with the news soon.</p>
                        </div>
                        <h3>Tell me more about you...</h3>
                        <p>Headquartered in Delhi and growing healthy food options in over 1000 acres of Greenergy farms that are spread across India, the venture is backed by experts both from the commercial side and product side. </p>
                        <p>We offer high quality hand picked organic fruits and vegetables straight from the farm to your home along with a line-up of ayurvedic products. </p>
                        <p>We have put our mind, body and soul to create a positive change in our collective health quotient and environment. We use hydroponic, aeroponic methods and zero-pesticides in our organic farming where we save up to 95% of water as compared to regular farming.</p>
                        <p>We have geologists, nutritionists and paediatricians on board. And we also strongly believe that our farmers are also one of the best experts. We listen to them carefully and follow their advice. </p>
                        <div class="connect-us bg-box">
                            <h3>How can we connect with us?</h3>
                            <p>We can be contacted through:</p>
                            <p><i class="fa fa-whatsapp"></i><span>Whatsapp:</span><strong>+91 8448695718 </strong></p>
                            <p><i class="fa fa-globe"></i><span>Website:</span><strong><a href="javascript:;">www.greenergy.me</a></strong></p>
                            <p class="cntnt-m"><i class="fa fa-envelope"></i><span>Email: </span><strong><a href="mailto:info@greenergy.me">info@greenergy.me</a></strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="back-arrow">
                        <a href="{{url('/')}}"><img src="{{asset('greenergy-new-theme/images/arrow.png')}}"></a>
                    </div>

                    <div class="footer-socail">
                        <ul>
                            <li><a href="javascript:;"><i class="fa fa-facebook"></i>Facebook</a></li>
                            <li><a href="javascript:;"><i class="fa fa-instagram"></i>Instagram</a></li>
                            <li><a href="javascript:;"><i class="fa fa-whatsapp"></i>WhatsApp</a></li>
                        </ul>
                        <p>@2020<a href="javascript:;"> greenergy.me</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</body>
</html>
