@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">My Wallet</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">My Wallet</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!--wallet-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-8">
                    <div class="card-body bg-white mb-4">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="fr966">
                                    <div class="_3nWSp"><span class="_2wFvp">₹</span><input type="text" name="" placeholder="Enter Amount" value=""><button data-toggle="modal" data-target="#exampleModal" name="subject">Add Money</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 d-flex">
                                <div class="frttl">
                                    <!-- heading -->
                                    <h6 class="text-muted mb-0">Total Balance:</h6>
                                    <!-- text -->
                                    <p class="mb-0 font-size-sm font-weight-bold txt-clr ml-1">&#x20B9;1000.0</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- order-items -->
                    <div class="card style-2 mb-4">
                        <div class="card-header">
                            <h4 class="mb-0">Order Item 3</h4>
                        </div>
                        <div class="card-body">
                            <ul class="inner-items">
                                <li>
                                    <div class="row align-items-center">
                                        <div class="col-4 col-md-3 col-xl-2">
                                            <a href="#">
                                                <img src="{{ asset('greenergy-latest/images/fruit.png') }}" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="col">
                                            <p class="mb-2 font-size-sm font-weight-normal">
                                                <a style="font-weight: 500;" href="product.html">Fresh Ukkio Pineapple</a> <br>
                                                <span style="font-weight: 500;" class="txt-clr2">&#x20B9;80.00</span>
                                            </p>
                                            <div class="font-size-sm text-muted">
                                                Weight: 2kg <br>
                                                Farm: Grocery Food Farm
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row align-items-center">
                                        <div class="col-4 col-md-3 col-xl-2">
                                            <a href="#">
                                                <img src="{{ asset('greenergy-latest/images/fruit3.png') }}" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="col">
                                            <p class="mb-2 font-size-sm font-weight-normal">
                                                <a style="font-weight: 500;" href="product.html">Fresh Ukkio Pineapple</a> <br>
                                                <span style="font-weight: 500;" class="txt-clr2">&#x20B9;80.00</span>
                                            </p>
                                            <div class="font-size-sm text-muted">
                                                Weight: 2kg <br>
                                                Farm: Grocery Food Farm
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row align-items-center">
                                        <div class="col-4 col-md-3 col-xl-2">
                                            <a href="#">
                                                <img src="{{ asset('greenergy-latest/images/fruit4.png') }}" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="col">
                                            <p class="mb-2 font-size-sm font-weight-normal">
                                                <a style="font-weight: 500;" href="product.html">Fresh Ukkio Pineapple</a> <br>
                                                <span style="font-weight: 500;" class="txt-clr2">&#x20B9;80.00</span>
                                            </p>
                                            <div class="font-size-sm text-muted">
                                                Weight: 2kg <br>
                                                Farm: Grocery Food Farm
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- total-items -->
                    <div class="card style-2 mb-4">
                        <div class="card-header">
                            <h4 class="mb-0">Total Order</h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-group list-group-sm list-group-flush-y list-group-flush-x">
                                <li class="list-group-item d-flex px-0">
                                    <span>Subtotal</span>
                                    <span class="ml-auto">&#x20B9;140.00</span>
                                </li>

                                <li class="list-group-item d-flex px-0">
                                    <span>Tax</span>
                                    <span class="ml-auto">&#x20B9;02.00</span>
                                </li>

                                <li class="list-group-item d-flex px-0">
                                    <span>Shipping</span>
                                    <span class="ml-auto">&#x20B9;15.10</span>
                                </li>

                                <li class="list-group-item d-flex font-size-lg font-weight-bold px-0">
                                    <span class="txt-clr" style="font-weight: 500;">Total</span>
                                    <span class="ml-auto txt-clr" style="font-weight: 500;">&#x20B9;177.00</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- shipping & billing -->
                    <div class="card style-2">
                        <div class="card-header">
                            <h4 class="mb-0">Shipping &amp; Billing  Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <!-- Heading -->
                                    <p class="mb-2 fnt-weight-5">
                                        Billing Address:
                                    </p>

                                    <p class="mb-7 mb-md-0">
                                        A 104 Sector 58, <br>
                                        Delhi, <br>
                                        Uttar Pradesh, <br>
                                        201301, <br>
                                        95120258010
                                    </p>

                                </div>

                                <div class="col-12 col-md-4">
                                    <!-- Heading -->
                                    <p class="mb-2 fnt-weight-5">
                                        Shipping Address:
                                    </p>

                                    <p class="mb-7 mb-md-0">
                                        A 104 Sector 58, <br>
                                        Delhi, <br>
                                        Uttar Pradesh, <br>
                                        201301, <br>
                                        95120258010
                                    </p>
                                </div>

                                <div class="col-12 col-md-4">

                                    <!-- Heading -->
                                    <p class="mb-2 fnt-weight-5">
                                        Shipping Method:
                                    </p>

                                    <p class="mb-4 text-gray-500">
                                        Standart Shipping <br>
                                        (5 - 7 days)
                                    </p>

                                    <!-- Heading -->
                                    <p class="mb-2 fnt-weight-5">
                                        Payment Method:
                                    </p>

                                    <p class="mb-0">
                                        Wallet
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="add-payments">
                <span class="mod-close" data-dismiss="modal" aria-hidden="true"><i class="ti-close"></i></span>
                <div class="modal-body">
                    <h4 class="modal-header-title">Add Credit Card</h4>
                    <div class="login-form">
                        <div class="form-row py-2">
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label class="bold-simple">Card Number</label>
                                    <input class="form-control form-control-sm" id="checkoutPaymentCardNumber" type="text" placeholder="Card Number *" required="">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label class="bold-simple">Name on Card</label>
                                    <input class="form-control form-control-sm" id="checkoutPaymentCardName" type="text" placeholder="Name on Card *" required="">
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group mb-md-0">
                                    <label class="bold-simple">Month</label>
                                    <select class="custom-select form-control" id="checkoutPaymentMonth">
                                        <option>January</option>
                                        <option>February</option>
                                        <option>March</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group mb-md-0">
                                    <label class="bold-simple">Year</label>
                                    <select class="custom-select form-control" id="checkoutPaymentCardYear">
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label class="bold-simple">Year</label>
                                    <div class="input-group input-group-merge">
                                        <input class="form-control form-control-sm" id="checkoutPaymentCardCVV" type="text" placeholder="CVV *" required="">
                                        <div class="input-group-append">
                                            <span class="input-group-text" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="The CVV Number on your credit card or debit card is a 3 digit number on VISA, MasterCard and Discover branded credit and debit cards." data-original-title="" title="">
                                            <i class="ti-help"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn3 btn-theme full-width">Save Card</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->
@stop
