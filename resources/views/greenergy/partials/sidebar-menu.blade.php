<div class="col-lg-4 col-md-3">
    <nav class="side-list mb-10 mb-md-0">
        <!-- mobile-view -->
        <div class="dropdown d-none1">
            <a class="w-100 p-125em btn btn-secondary dropdown-toggle res-dropdown" href="{{ url('account-settings') }}" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Account-settings
            </a>
            <div class="dropdown-menu res-width bd-clr pt-0" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item res-dropdown p-125em border border-bottom @if(request()->path() === 'order-history')) active @endif" href="{{ url('/order-history') }}">Order History</a>
                <a class="dropdown-item p-125em res-dropdown border border-bottom @if(request()->path() === 'order-tracking')) active @endif" href="{{ url('/order-tracking') }}">Order Tracking</a>
                <a class="dropdown-item p-125em res-dropdown border border-bottom @if(request()->path() === 'wishlist')) active @endif" href="{{ url('/wishlist') }}">Wishlist</a>
                <a class="dropdown-item p-125em res-dropdown border border-bottom @if(request()->path() === 'wallet')) active @endif" href="{{ url('/wallet') }}">Wallet</a>
                <a class="dropdown-item p-125em res-dropdown border border-bottom @if(request()->path() === 'my-order')) active @endif" href="{{ url('/my-order') }}">My Order</a>
                <a class="dropdown-item p-125em res-dropdown border border-bottom @if(request()->path() === 'payment-methods')) active @endif" href="{{ url('/payment-methods') }}">Payments Methods</a>
            </div>
        </div>
        <!-- end -->
        <div class="list-group list-group-sm list-group-strong list-group-flush-x" id="myDIV">
            <a class="list-group-item list-group-item-action @if(request()->path() === 'my-order')) active @endif" href="{{ url('/my-order') }}">My Order</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'wallet')) active @endif" href="{{ url('/wallet') }}">Wallet</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'order-history')) active @endif" href="{{ url('/order-history') }}">Order History</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'order-tracking')) active @endif" href="{{ url('/order-tracking') }}">Order Tracking</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'wishlist')) active @endif" href="{{ url('/wishlist') }}">Wishlist</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'account-settings')) active @endif" href="{{ url('/account-settings') }}">Account Settings</a>
            <a class="list-group-item list-group-item-action dropright-toggle @if(request()->path() === 'payment-methods')) active @endif" href="{{ url('/payment-methods') }}">Payment Methods</a>
            <a class="list-group-item list-group-item-action dropright-toggle " href="#">Logout</a>
        </div>
    </nav>
</div>
