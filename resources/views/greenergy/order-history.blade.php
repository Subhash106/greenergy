@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">My All Orders</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">My Order</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">My Order</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!-- order-history-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-9">
                    <!-- order-items -->
                    <div class="card style-2">
                        <div class="card-header">
                            <h4 class="mb-0">Total Order</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-element">
                                <table class="table2">
                                    <thead>
                                    <tr>
                                        <th class="txt-clr" scope="col">Product</th>
                                        <th class="txt-clr" scope="col">Order No.</th>
                                        <th class="txt-clr" scope="col">Shipping Date</th>
                                        <th class="txt-clr" scope="col">Price</th>
                                        <th class="txt-clr" scope="col">Status</th>
                                        <th class="txt-clr" scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="tbl_cart_product">
                                                <div class="tbl_cart_product_thumb m-0">
                                                    <img src="{{ asset('greenergy-latest/images/fruit.png') }}" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </th>
                                        <td>8910563</td>
                                        <td>10 Jul 2020</td>
                                        <td>&#x20B9;89.00</td>
                                        <td>In Processing</td>
                                        <td><a href="#" class="btn btn-sm btn-theme">Track Order</a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="tbl_cart_product">
                                                <div class="tbl_cart_product_thumb m-0">
                                                    <img src="{{ asset('greenergy-latest/images/fruit4.png') }}" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </th>
                                        <td>8910563</td>
                                        <td>10 Jul 2020</td>
                                        <td>&#x20B9;89.00</td>
                                        <td>In Processing</td>
                                        <td><a href="#" class="btn btn-sm btn-theme">Track Order</a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="tbl_cart_product">
                                                <div class="tbl_cart_product_thumb m-0">
                                                    <img src="{{ asset('greenergy-latest/images/fruit3.png') }}" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </th>
                                        <td>8910563</td>
                                        <td>10 Jul 2020</td>
                                        <td>&#x20B9;89.00</td>
                                        <td>In Processing</td>
                                        <td><a href="#" class="btn btn-sm btn-theme">Track Order</a></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="tbl_cart_product">
                                                <div class="tbl_cart_product_thumb m-0">
                                                    <img src="{{ asset('greenergy-latest/images/fruit4.png') }}" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                        </th>
                                        <td>8910563</td>
                                        <td>10 Jul 2020</td>
                                        <td>&#x20B9;89.00</td>
                                        <td>In Processing</td>
                                        <td><a href="#" class="btn btn-sm btn-theme">View Order</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
@stop
