<!DOCTYPE html>
<html lang="en">
<head>
    <title>Greenergy - Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('greenergy-new-theme/css/style.css')}}">
</head>
<body>
{{--@if($errors->any())
    {{print_r($errors->all())}}
@endif--}}
<div class="layout">
    <!--header start-->
    <header class="header-home">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo">
                        <img src="{{asset('greenergy-new-theme/images/logo.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--body container start-->
    <section class="welcome-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="welcome-text">
                        <h2>Welcome to the best of life.</h2>
                        <p>We offer high quality handpicked organic fruits and vegetables straight from the farm to your
                            home along with a line-up of ayurvedic products. We use hydroponic, aeroponic methods and
                            zero-pesticides in our organic farming where we save up to 95% of water as compared to
                            regular farming.<br><br>
                            It's a membership only platform where we make no compromises in the quality of our products
                            and ensure you the most nutritious and healthiest fruits and vegetables in all seasons.
                        </p>
                    </div>
                    <div class="why-green-box">
                        <h2>Why Greenergy.Me?</h2>
                        <ul>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g1.png')}}">100% Organic. 100%
                                Transparency.
                            </li>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g2.png')}}">Led by Experts</li>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g3.png')}}">Farm to Table</li>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g4.png')}}">Fresh Delivery always.
                            </li>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g5.png')}}">Designed for everyday
                                nutrition
                            </li>
                            <li><img src="{{asset('greenergy-new-theme/images/why-g6.png')}}">Sustainable practices</li>
                        </ul>
                    </div>
                    <div class="timer">
                        <p>We are coming soon to serve you in </p>
                        <ul>
                            <li id="demo"></li>
                        </ul>
                    </div>
                    <!--<div class="countDown">
                        We are coming soon to serve you in

                        <h1 id="demo"></h1>
                    </div>-->
                    <div class="video-link">
                        <a href="javascript:;"><img data-toggle="modal" data-target="#video-linK"
                                                    src="{{asset('greenergy-new-theme/images/video-ply-btn.png')}}">Greenergy
                            says hello!</a>
                        <div class="modal fade" id="video-linK" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <iframe width="100%" height="100%"
                                                src="https://www.youtube.com/embed/Q_dhqCSkTT8" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="wantto-form-box">
                        <h2>Want to know more about our offerings?</h2>
                        <p>Write to us here and we will get in touch
                            with you.
                        </p>
                        <div class="form-container">
                            <form method="POST" action="{{route('call-requests.store')}}" id="form-cta-subscribe-2"
                                  novalidate>
                                @csrf
                                <div class="cs-notifications">
                                    <div class="cs-notifications-content"></div>
                                    <div id="successMessage"></div>
                                    <div id="errorMessage"></div>
                                </div><!-- .cs-notifications end -->
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Name"
                                           value="{{old('name')}}" required="">
                                </div>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input id="email" type="text" class="form-control" name="email" placeholder="Email"
                                           value="{{old('email')}}">
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    <input id="phone" type="text" class="form-control" name="phone"
                                           placeholder="Phone Number" value="{{old('phone')}}" required maxlength="10">
                                </div>
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror

                                <!--<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    <input id="phone" type="text" class="form-control" name="city"
                                           placeholder="City" required="" value="{{old('pincode')}}">
                                </div>-->

                                <div class="input-group select-box">
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    <select class="form-control" name="city">
                                        <option value="delhi-ncr" selected>Delhi NCR</option>
                                        <option value="mumbai">Mumbai</option>
                                        <option value="jaipur">Jaipur</option>
                                        <option value="kolkata">Kolkata</option>
                                    </select>
                                </div>
                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input id="pincode" type="text" class="form-control" name="pincode"
                                           value="{{old('pincode')}}" placeholder="Pincode" required
                                           maxlength="6">
                                </div>
                                @error('pincode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror

                                {{--<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar-times-o"></i></span>
                                    <input type="datetime-local" name="call_time" id="call_time"
                                           value="{{old('call_time')}}" class="form-control" required
                                           maxlength="6">
                                </div>
                                @error('call_time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $message }}</strong>
                                    </span>
                                @enderror--}}
                                <div class="save-btn">
                                    <input style="width: 100%;" type="submit" value="SUBMIT" name="submit" class="btn btn-success">
                                    {{--<a href="javascript:;" data-toggle="modal" data-target="#success-popup">Submit</a>--}}
                                    <!--popup success-->
                                    <div class="modal fade success-popup-main" id="success-popup" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="success-popup">
                                                        <img
                                                            src="{{asset('greenergy-new-theme/images/check-icon.png')}}">
                                                        <h4>Thank You</h4>
                                                        <p>for submitting your query. Someone from our team will contact
                                                            you shortly.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="faq-link">
                            <a href="{{url('/faq')}}"><i class="fa fa-angle-right"></i> faq</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script>
        // Set the date we're counting down to
        var countDownDate = new Date("July 15, 2020 00:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {
            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h: "
                + minutes + "m: " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "WELCOME TO GEENERGY";
            }
        }, 1000);
</script>
<script>
    $(document).ready(function () {
        @if(session('success'))
            $('#success-popup').addClass('in show');
        @endif

        $(document).on('click', '#success-popup', function () {
            $('#success-popup').removeClass('in show');
        });
    })
</script>
</body>
</html>
