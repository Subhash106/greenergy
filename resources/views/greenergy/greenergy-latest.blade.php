 <!DOCTYPE html>
<html lang="en">

<head>
    <title>Greenergy</title>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="{{ asset('greenergy-latest/images/fav.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('greenergy-latest/css/fullpage.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('greenergy-latest/css/style.css') }}">
</head>

<body class="loading">
<div class="overly-prdct"><a href="#0" class="cd-close"></a></div>
<div id="fullpage">
    <div class="section active" id="section0">

        <header class="home-header">
            <div class="overly"></div>
            <video class="video" autoplay loop width="100%" id="myVideo6">
                <source src="{{ asset('greenergy-latest/images/movie.mp4') }}" type="video/mp4">
                <source src="{{ asset('greenergy-latest/images/mov_bbb.ogg') }}" type="video/ogg">
            </video>

            <div class="inner-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="logo">
                                <img src="{{ asset('greenergy-latest/images/logo.png') }}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="right-search">
                                <ul>
                                    <li><a href="#"><img src="{{ asset('greenergy-latest/images/search-icon.png') }}"></a></li>
                                    <li><a href="#"><img src="{{ asset('greenergy-latest/images/logo-icon.png') }}"></a></li>
                                </ul>
                                <div class="search-container">
                                    <form action="#">
                                        <input type="text" placeholder="Search.." name="search">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="banner-caption">
                                <div>
                                    <h1>Welcome to <strong>Greenergy.Me</strong>
                                        Welcome to <strong>The Best of Life.</strong></h1>
                                    <a href="#" onclick="playVid6()">Watch the brand film ></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="down-icon">
                    <div><a href="#"><img src="{{ asset('greenergy-latest/images/drop-icon.png') }}"></a></div>
                </div>
            </div>

        </header>




    </div>
    <div class="section" id="section1">
        <section class="our-resolve" id="downsec">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="slider-outer">
                            <div class="owl-carousel owl-carousel1 owl-theme">
                                <div class="item">
                                    <h3>Our Resolve</h3>
                                    <p>Greenergy.Me is all about serving you with the best Mother Nature has to offer. From sun-kissed
                                        farms to sustainable growing, we are ensuring the energy of goodness is delivered to you just the
                                        way it is intended to be.</p>
                                    <a href="javascript:;">Why Us?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <div class="section" id="section2">
        <section class="organic-farm-slider">
            <div class="tab-menu">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#organic-products">Organic products</a></li>
                    <li><a data-toggle="tab" href="#from-farm">From the farm</a></li>
                </ul>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slider-outer">
                            <div class="tab-content">
                                <div id="organic-products" class="tab-pane fade in active">
                                    <div class="owl-carousel owl-carousel2 owl-theme">

                                        {{--<div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/honey-1772792_1280.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Honey</span>--}}{{--Tomato 90g--}}{{--</h3>
                                            </div>
                                        </div>--}}
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/indian-spices-829198_1280.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Spices</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>

                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/honey-1006972_1280.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Honey</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Lentils.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Lentils</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Rice.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Rice</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="from-farm" class="tab-pane fade">
                                    <div class="owl-carousel owl-carousel2 owl-theme imglist">

                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/farm-img1.png') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Potato</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/farm-img2.png') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Tomato</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>

                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Green Chillies.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Green Chillies</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Onion.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Onion</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Lettuce.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Lettuce</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="prdct-img">
                                                <img src="{{ asset('greenergy-latest/images/Spinach.jpg') }}">
                                                <span href="javascript:;" class="icon-click"><i class="fa fa-plus"></i></span>
                                                <h3><span>Spinach</span>{{--Tomato 90g--}}</h3>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="section" id="section3">
        <section class="tab-on-video">
            <video class="video" autoplay loop width="100%" id="myVideo5">
                <source src="{{ asset('greenergy-latest/images/movie.mp4') }}" type="video/mp4">
                <source src="{{ asset('greenergy-latest/images/mov_bbb.ogg') }}" type="video/ogg">
            </video>


            <div class="play-btn">
                <div class="inner">
                    <div class="tab-menu">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#organic-products-tab">{{ $organic_products->name }}</a></li>
                            <li><a data-toggle="tab" href="#from-farm-tab">{{ $from_the_farm->name }}</a></li>
                        </ul>
                    </div>
                    <div class="container" id="products-section">
                        <div class="tab-content">
                            <div id="organic-products-tab" class="tab-pane fade in active">
                                <div class="farm-prdct-menu">
                                    <ul>
                                        @foreach($organic_products->categories as $category)
                                        <li><a href="javascript:;" onclick="myFunction('{{ $category->video_name }}')"><i class="fa fa-angle-left"></i>{{ $category->name }}</a>
                                            <ul>
                                                @foreach($category->products as $product)
                                                    <li><a href="javascript:;"{{-- class="buy-product"--}} product_id="{{ $product->id }}" {{--onclick="myFunction('Atta.mp4')"--}}>{{ $product->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div id="from-farm-tab" class="tab-pane fade">
                                <div class="farm-prdct-menu">
                                    <ul>
                                        @foreach($from_the_farm->categories as $category)
                                            <li><a href="javascript:;" onclick="myFunction('{{ $category->video_name }}')"><i class="fa fa-angle-left"></i>{{ $category->name }}</a>
                                                <ul>
                                                    @foreach($category->products as $product)
                                                        <li><a href="javascript:;"{{-- class="buy-product"--}} product_id="{{ $product->id }}" {{--onclick="myFunction('Atta.mp4')"--}}>{{ $product->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn-play1" onclick="playVid5()"><img src="{{ asset('greenergy-latest/images/play-btn.png') }}"></button>
                    <button  class="btn-pause1" onclick="pauseVid5()"><img src="{{ asset('greenergy-latest/images/pause.png') }}"></button>

                </div>
            </div>

        </section>
    </div>

    <div class="section" id="section4">
        <section class="why-greenergy">
            <div class="container">
                <div class="outer">
                    <div class="inner">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-6">
                                <img src="{{ asset('greenergy-latest/images/why-green.jpg') }}">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-6">
                                <div class="detl">
                                    <h4>Why Greenergy.Me?</h4>
                                    <ul>
                                        <li>- 100% Organic</li>
                                        <li>- 100% Transparency</li>
                                        <li>- Led by Experts</li>
                                        <li>- Farm to Table</li>
                                        <li>- Fresh Delivery always</li>
                                        <li>- Designed for everyday nutrition</li>
                                        <li>- Sustainable practices</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="section" id="section5">
        <section class="about-us">
            <div class="container">
                <div class="outer">
                    <div class="inner">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>About Us</h3>
                                <p>We have put our mind, body and soul to create a positive change in our collective health quotient and
                                    environment. We offer high quality handpicked organic fruits and vegetables straight from the farm to
                                    your home along with a line-up of ayurvedic products.</p>
                                <p>It's a membership only platform where we make no compromises in the quality of our products and
                                    ensure you the most nutritious and healthiest fruits and vegetables in all seasons.</p>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-lg-6">
                                        <a href="javascript:;">Our certifications </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-lg-6">
                                        <a href="javascript:;"> Our modern techniques</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="section" id="section6">
        <section class="Weuse-hydroponic-video" style="background: url('{{asset('greenergy-latest/images/plants.jpg')}}'); background-size: cover">

            {{--<video width="100%" id="myVideo2">
                <source src="{{ asset('greenergy-latest/images/movie.mp4') }}" type="video/mp4">
                <source src="{{ asset('greenergy-latest/images/mov_bbb.ogg') }}" type="video/ogg">
            </video>--}}

            <div class="play-btn">
                <div>
                    <h4>We use hydroponic, aeroponic methods and zero-pesticides in our organic farming where we save up to 95% of
                        water as compared to regular farming.</h4>
                    {{--<button class="btn-play2" onclick="playVid2()"><img src="{{ asset('greenergy-latest/images/play-btn.png') }}"></button>
                    <button  class="btn-pause2" onclick="pauseVid2()"><img src="{{ asset('greenergy-latest/images/pause.png') }}"></button>--}}
                </div>
            </div>

        </section>
    </div>

    <div class="section" id="section7">
        <section class="there-is-more">
            <div class="container">
                <div class="outer">
                    <div class="inner">
                        <div class="row">
                            <div class="col-sm-12 col-sm-12 col-lg-6">
                                <h3>There is more to us</h3>
                                <p>We are here to be more than just any organic farm or food company. We are here to make a difference.
                                    By engaging communities and seek their participation. As we grow the nature’s gift, we will bring the
                                    next generation up, close and personal to what we are engaged in. Our ultimate goal is to usher in the
                                    organic revolution in the city with technologies and process for everyone to grow organic food, right
                                    in their home.</p>
                            </div>
                            <div class="col-sm-12 col-sm-12 col-lg-6">
                                <div style="width: 100%;text-align:center">
                                    <img src="https://mbj-sub-bucket.s3.ap-south-1.amazonaws.com/Plant+in+hand.jpg" alt="Plant in hand" style="height: 440px; width:440px">
                                </div>
                                {{--<video width="100%" height="480x"--}}{{-- poster="{{ asset('greenergy-latest/images/there-more-banner.jpg') }}"--}}{{-- id="myVideo4">
                                    <source src="{{ asset('greenergy-latest/images/plant.mp4') }}" type="video/mp4">
                                    <source src="{{ asset('greenergy-latest/images/mov_bbb.ogg') }}" type="video/ogg">
                                </video>
                                <div class="play-btn2">
                                    <button class="btn-play3" onclick="playVid4()"><img src="{{ asset('greenergy-latest/images/play-btn.png') }}"></button>
                                    <button  class="btn-pause3" onclick="pauseVid4()"><img src="{{ asset('greenergy-latest/images/pause.png') }}"></button>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="section" id="section8">
        <footer class="footer">
            <div class="footer-logo-icon">
                <a href="javascript:;">
                    <img src="{{ asset('greenergy-latest/images/logo-icon.png') }}">
                </a>
            </div>
            <div class="container">
                <div class="outer">
                    <div class="inner">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3>We are all ears for you.<br><br> Contact us</h3>
                            </div>
                            <div class="col-sm-6">
                                <address>
                                    <p><span>Whatsapp - </span> +91-8595745991</p>
                                    <p><span>Website -</span> www.greenergy.me</p>
                                    <p><span>Email - </span> info@greenergy.me</p>
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="socail">
                                    <h3>Join Us:</h3>
                                    <ul>
                                        <li><a href="https://www.facebook.com/greenergy.me/?modal=admin_todo_tour"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://instagram.com/greenergy.me?igshid=8q25d28rpmkx"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=918595745991&text=&source=&data=&app_absent=" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-links">
                <ul>
                    <li><a href="{{ asset('FAQs.pdf') }}" target="_blank">FAQs</a></li>
                    <li><a href="javascript:;">Grow the Tribe</a></li>
                    <li><a href="javascript:;">Privacy Policy</a></li>
                </ul>
            </div>
        </footer>
    </div>
</div>

<!--popup product-->



<!--popup product-->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="{{ asset('greenergy-latest/js/fullpage.js') }}"></script>
<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
<script src="{{ asset('greenergy-latest/js/custom.js') }}"></script>
</body>

<script>
    $(document).ready(function () {
            $('#products-section').on('click', '.buy-product', function () {
                let product_id = $(this).attr('product_id');
                let url = "{{ route('get-product', ':product_id') }}"
                url = url.replace(':product_id', product_id);
                let self =$(this);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function(response){
                        if(response.status === 'success'){
                            addPopUpContent(response.data);
                            $(".mypopup").fadeIn(1000);
                            $(".mypopup .left-img-popup").addClass("img-animation");
                            $(".mypopup .left-img-popup").removeClass("remove-img-animation");
                            $(".right-popup-contnt").addClass("right-contnt-anmation");
                        }else{
                            alert('something went wrong');
                        }
                    }
                });
            });



    });

    function addPopUpContent(data) {
        let packages = '';
        let package_details = data.packages;
        for (let i=0 ; i<package_details.length; i++) {
            let classname = (i)? '':'active';
            packages += '<li class='+classname+'><a href="javascript:;"><span>'+package_details[i].units+' '+package_details[i].measuring_unit +'</span></a></li>';
        }
        $('body').append(`
        <div class="mypopup">
    <span class="close fa fa-close"></span>
    <section class="container custm-container">
        <div class="left-img-popup">
            <img src="`+data.image+`">
        </div>
        <div class="right-popup-contnt">
            <h3><span>`+data.category+`</span>`+data.name+`</h3>
            <div class="price-box">
                <p class="price"><i class="fa fa-inr"></i>`+data.selling_price+`/-<span class="discount"><i class="fa fa-inr"></i>`+data.price+`</span></p>
            </div>
            <p class="tax"> You Save: 40% (Inclusive of all taxes</p>
            <div class="weight">
                <label>Weight</label>
                <ul>
                    `+packages+`
                </ul>
            </div>

            <div class="delivery-time">
                <p>Standard Delivery: Tomorrow 7:00AM - 7:00pm</p>
            </div>

            <div class="add-cart-btn">
                <ul>
                    <li><a href="{{ url('/cart') }}"><span>1</span><i class="fa fa-shopping-basket"></i>Add to Cart</a></li>
                </ul>
            </div>
        </div>
        <div class="product-dtl-arrow">
            <div><a href="#prdctdtl"><span>Product Details</span><img src="{{ asset('greenergy-latest/images/drop-icon.png') }}"></a></div>
        </div>
    </section>


    <section class="product-dtl" id="prdctdtl">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Ingredients</h3>
                    <p>`+data.ingredients+`</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <span class="border-top"></span>
                </div>
                <div class="col-sm-6">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width:48%">Product information</th>
                            <th style="width:4%"></th>
                            <th style="width:48%">100g</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Energy (KJ):</td>
                            <td>--</td>
                            <td>`+data.energy+`</td>
                        </tr>
                        <tr>
                            <td>Protein:</td>
                            <td>--</td>
                            <td>`+data.protein+`</td>
                        </tr>
                        <tr>
                            <td>Dietary Fiber:</td>
                            <td>--</td>
                            <td>`+data.dietary_fiber+`</td>
                        </tr>
                        <tr>
                            <td>Carbohydrates:</td>
                            <td>--</td>
                            <td>`+data.carbohydrates+`</td>
                        </tr>
                        <tr>
                            <td>Sugar:</td>
                            <td>--</td>
                            <td`+data.sugar+`</td>
                        </tr>
                        <tr>
                            <td>Fat in total:</td>
                            <td>--</td>
                            <td>`+data.total_fat+`</td>
                        </tr>
                        <tr>
                            <td>Saturated fat:</td>
                            <td>--</td>
                            <td>`+data.saturated_fat+`</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <div class="progres-bar-main">
                        <div class="form-group">
                            <label>Calories<span>`+data.calories+`</span></label>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                                    70%
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Protein<span>`+data.protein+`</span></label>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                                    70%
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Fiber<span>`+data.fiber+`</span></label>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                    70%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
        `);
        $(".mypopup .close").click(function () {
            $(".mypopup").fadeOut(1500);
            $(".mypopup .left-img-popup").removeClass("img-animation");
            $(".mypopup .left-img-popup").addClass("remove-img-animation");
            $(".right-popup-contnt").removeClass("right-contnt-anmation");
        });
    }
</script>
</html>
