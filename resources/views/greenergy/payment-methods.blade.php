@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">Payment Method</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">Payment Method</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!-- payment-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-9">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="add-payment-card">
                                <div class="ap-card-header mb-2">
                                    <div class="card-header-thumb">
                                        <img src="{{ asset('greenergy-latest/images/card.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="ml-auto">
                                        <label>Expiry Date</label>
                                        <div class="card-caption">
                                            02/22
                                        </div>
                                    </div>
                                </div>
                                <div class="ap-card-body">
                                    <div class="ml-auto mb-3">
                                        <label>Card Number</label>
                                        <div class="card-caption">
                                            4016 - 1242 - 6585 - 7200
                                        </div>
                                    </div>
                                    <div class="ap-card-footer">
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="ml-auto">
                                                    <label>Card Holder</label>
                                                    <div class="card-caption">
                                                        Yash Kumar
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="ml-auto">
                                                    <label>CCV</label>
                                                    <div class="card-caption">
                                                        ***<i class="ti-info info-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="add-payment-card">
                                <div class="ap-card-header mb-2">
                                    <div class="card-header-thumb">
                                        <img src="{{ asset('greenergy-latest/images/card.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="ml-auto">
                                        <label>Expiry Date</label>
                                        <div class="card-caption">
                                            02/22
                                        </div>
                                    </div>
                                </div>
                                <div class="ap-card-body">
                                    <div class="ml-auto mb-3">
                                        <label>Card Number</label>
                                        <div class="card-caption">
                                            4016 - 1242 - 6585 - 7200
                                        </div>
                                    </div>
                                    <div class="ap-card-footer">
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="ml-auto">
                                                    <label>Card Holder</label>
                                                    <div class="card-caption">
                                                        Yash Kumar
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="ml-auto">
                                                    <label>CCV</label>
                                                    <div class="card-caption">
                                                        ***<i class="ti-info info-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="add-payment-card">
                                <div class="ap-card-header mb-2">
                                    <div class="card-header-thumb">
                                        <img src="{{ asset('greenergy-latest/images/card.png') }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="ml-auto">
                                        <label>Expiry Date</label>
                                        <div class="card-caption">
                                            02/22
                                        </div>
                                    </div>
                                </div>
                                <div class="ap-card-body">
                                    <div class="ml-auto mb-3">
                                        <label>Card Number</label>
                                        <div class="card-caption">
                                            4016 - 1242 - 6585 - 7200
                                        </div>
                                    </div>
                                    <div class="ap-card-footer">
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="ml-auto">
                                                    <label>Card Holder</label>
                                                    <div class="card-caption">
                                                        Yash Kumar
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="ml-auto">
                                                    <label>CCV</label>
                                                    <div class="card-caption">
                                                        ***<i class="ti-info info-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="add-payment-card center">
                                <div class="add-pay-card">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal" class="btn btn-pay"><i class="ti-credit-card"></i></a>
                                </div>
                                <span>Add Debit/Credit card</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
@stop
