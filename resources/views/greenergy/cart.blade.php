@extends('layouts.frontend')

@section('content')
    <section>
        <div class="row ml-0 mr-0">
            <div class="col-md-9 p-0 ">
                <div class="cart-product bg-dark-pink border-bottom border-white ">
                    <div class="header-title">
                        <h3 class="text-white mb-4">Shopping Cart</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table text-white table-borderless">
                            <tr>
                                <th>Product</th>
                                <th></th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Weight</th>
                                <th>Total</th>
                            </tr>
                            <tr>
                                <td>
                                    <div class="product-detail ">
                                        <figure class="product-avtar bg-white rounded-circle mr-auto mb-0"><img src="{{ asset('greenergy-latest/images/popup-img.png') }}"
                                                                                                                alt=""></figure><a class="neumo rounded-circle remove-icon"><span
                                                class=" rounded-circle ">X</span></a>
                                    </div>
                                </td>
                                <td>
                                    <h5 class="product-name mb-0">Potato<span class="d-block">Vegetable<span></h5>
                                </td>
                                <td>&#8377;500/-</td>
                                <td>
                                    <div class="handle-counter" id="handleCounter">
                                        <button class="counter-minus btn"><span class="ti-minus"></span></button>
                                        <input type="text" value="3" id="">
                                        <button class="counter-plus btn "><span class="ti-plus"></span></button>
                                    </div>
                                </td>
                                <td>1kg</td>
                                <td>500</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="product-detail ">
                                        <figure class="product-avtar bg-white rounded-circle mr-auto mb-0"><img src="{{ asset('greenergy-latest/images/popup-img.png') }}"
                                                                                                                alt=""></figure><a class="neumo rounded-circle remove-icon"><span
                                                class=" rounded-circle ">X</span></a>
                                    </div>
                                </td>
                                <td>
                                    <h5 class="product-name mb-0">Potato<span class="d-block">Vegetable<span></h5>
                                </td>
                                <td>&#8377;500/-</td>
                                <td>
                                    <div class="handle-counter" id="handleCounter">
                                        <button class="counter-minus btn"><span class="ti-minus"></span></button>
                                        <input type="text" value="3" id="">
                                        <button class="counter-plus btn "><span class="ti-plus"></span></button>
                                    </div>
                                </td>
                                <td>1kg</td>
                                <td>500</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="product-detail ">
                                        <figure class="product-avtar bg-white rounded-circle mr-auto mb-0"><img src="{{ asset('greenergy-latest/images/popup-img.png') }}"
                                                                                                                alt=""></figure><a class="neumo rounded-circle remove-icon"><span
                                                class=" rounded-circle ">X</span></a>
                                    </div>
                                </td>
                                <td>
                                    <h5 class="product-name mb-0">Potato<span class="d-block">Vegetable<span></h5>
                                </td>
                                <td>&#8377;500/-</td>
                                <td>
                                    <div class="handle-counter" id="handleCounter">
                                        <button class="counter-minus btn"><span class="ti-minus"></span></button>
                                        <input type="text" value="3" id="">
                                        <button class="counter-plus btn "><span class="ti-plus"></span></button>
                                    </div>
                                </td>
                                <td>1kg</td>
                                <td>500</td>
                            </tr>
                        </table>
                    </div>
                    <button class="neumo ml-auto btn mt-4"><span class="rounded-pill p-2 pl-5 pr-5">Add More Items</span></button>
                </div>
                <!-- cart-product end -->
                <div class="cart-product bg-dark-pink border-bottom border-white ">
                    <div class="header-title">
                        <h3 class="text-white mb-4">Who is Placing This order?</h3>
                    </div>
                    <div class="row mr-0 ml-0">
                        <div class="col-md-8">
                            <form action="">
                                <div class="form-group ">
                                    <label class="text-white">Email</label>
                                    <div class="form-box rounded-pill mb-4">
                                        <input type="email" placeholder="Example@gmail.com" class="rounded-pill p-3">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="text-white">Password</label>
                                    <div class="form-box rounded-pill mb-4">
                                        <input type="password" placeholder="password" class="rounded-pill p-3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="neumo btn "><span class="rounded-pill p-2 pl-5 pr-5">Add More Items</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- cart-product end -->
                <div class="cart-product bg-dark-pink border-bottom border-white ">
                    <div class="header-title">
                        <h3 class="text-white mb-4">Where would you like your order sent?</h3>
                    </div>
                    <div class="row mr-0 ml-0">
                        <div class="col-md-12">
                            <form action="">
                                <div class="form-row ">
                                    <div class="col-md-6">
                                        <label class="text-white">First Name<em>*</em></label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" placeholder="" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-white">Email<em>*</em></label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" placeholder="" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-white">Mobile No </label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" placeholder="" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-white">Mobile No </label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" placeholder="" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="text-white">Address</label>
                                        <div class="form-box rounded-pill mb-4">
                                            <textarea type="text" placeholder="" class="rounded-pill p-3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-white">City<em>*</em> </label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="text-white">Pincode<em>*</em></label>
                                        <div class="form-box rounded-pill mb-4">
                                            <input type="text" placeholder="" class="rounded-pill p-3">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="neumo btn"><span class="rounded-pill p-2 pl-5 pr-5">Add More Items</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- cart-product end -->
            </div>
            <!-- col-md-8 -->
            <div class="col-md-3 p-0 d-none  d-md-block d-lg-block">
                <div class="sidebar_widegs pt-5 p-3 ">
                    <h4 class="pb-3 h2 font-700">Amount</h4>
                    <ul class="cart-price">
                        <li class="cart-list">Sub Total<span class="ml-auto">Rs 196/-</span></li>
                        <li class="cart-list">Shipping<span class="ml-auto">Rs 50/-</span></li>
                        <li class="cart-list">Your Wallet<span class="ml-auto">Rs 50/-</span></li>
                        <li class="cart-list">Promocode<span class="ml-auto"><input type="text" placeholder="Enter Code"
                                                                                    minlength="4" maxlength="6" class="border-0"></span></li>

                    </ul>
                    <ul class="cart-price">
                        <li class="cart-list text-dark-gray font-700 ">Cart Total<span class="ml-auto">Rs 246/-</span></li>
                        <li class="cart-list"><button class="btn neumo btn-block "><span
                                    class="rounded-pill p-2">Checkout</span></button></li>
                    </ul>
                </div>
            </div>
            <div class=" p-0 sidebar-fixed">
                <div class="arrow-element"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>
                <div class="sidebar_widegs pt-5 p-3">
                    <h4 class="pb-3 h2 font-700">Amount</h4>
                    <ul class="cart-price">
                        <li class="cart-list">Sub Total<span class="ml-auto">Rs 196/-</span></li>
                        <li class="cart-list">Shipping<span class="ml-auto">Rs 50/-</span></li>
                        <li class="cart-list">Your Wallet<span class="ml-auto">Rs 50/-</span></li>
                        <li class="cart-list">Promocode<span class="ml-auto"><input type="text" placeholder="Enter Code"
                                                                                    minlength="4" maxlength="6" class="border-0"></span></li>

                    </ul>
                    <ul class="cart-price">
                        <li class="cart-list text-dark-gray font-700 ">Cart Total<span class="ml-auto">Rs 246/-</span></li>
                        <li class="cart-list"><button class="btn neumo btn-block "><span
                                    class="rounded-pill p-2">Checkout</span></button></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@stop
