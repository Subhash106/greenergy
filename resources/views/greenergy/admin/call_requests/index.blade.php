@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Call Requests</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Call Requests') }}
                    </div>

                    <div class="card-body">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>Pincode</th>
                                {{--<th>Call Time</th>--}}
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($call_requests as $key => $call_request)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$call_request->name}}</td>
                                    <td>{{$call_request->phone}}</td>
                                    <td>{{$call_request->email}}</td>
                                    <td>{{$call_request->city == 'delhi-ncr' ? 'Delhi NCR': ucfirst($call_request->city)}}</td>
                                    <td>{{$call_request->pincode}}</td>
                                    {{--<td>{{date('d M Y, h:i A', strtotime($call_request->call_time))}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$call_requests->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.pagination').addClass('justify-content-end')
        });
    </script>
@endpush
