@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">Racking Order: ODEXORD149</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">Shop</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">Racking Order</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!-- trracking-order-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-9 col-sm-12 col-12">
                    <div class="tracking-order">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="shipped_box">
                                    <span class="text-bold mr-2">Shipped via:</span> UPS
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="status_box">
                                    <span class="text-bold mr-2">Status:</span> Processing
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="deliver_box fnt-14px">
                                    <span class="text-bold mr-2">Delivered:</span> 20 August 2020
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <ul class="track_order_list mt-4">
                                    <li class="complete">
                                        <div class="trach_single_list">
                                            <div class="trach_icon_list"><i class="ti-write"></i></div>
                                            <div class="track_list_caption">
                                                <h4 class="mb-0">Order Placed</h4>
                                                <p class="txt-clr">We have received your order</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="processing">
                                        <div class="trach_single_list">
                                            <div class="trach_icon_list"><i class="ti-package"></i></div>
                                            <div class="track_list_caption">
                                                <h4 class="mb-0">Order Confirmed</h4>
                                                <p class="txt-clr">Your Order has been confirmed.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="trach_single_list">
                                            <div class="trach_icon_list"><i class="ti-thumb-up"></i></div>
                                            <div class="track_list_caption">
                                                <h4 class="mb-0">Order Processed</h4>
                                                <p class="txt-clr">We are preparing your order.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="trach_single_list">
                                            <div class="trach_icon_list"><i class="ti-gift"></i></div>
                                            <div class="track_list_caption">
                                                <h4 class="mb-0">Ready to Pickup</h4>
                                                <p class="txt-clr">Your order is ready for pickup.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
@stop
