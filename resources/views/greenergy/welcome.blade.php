<!doctype html>
<html lang="en-US">
<head>
    <title>GREENERGY - Welcome to the best of life</title>
    <!-- Meta
    ============================================= -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, intial-scale=1, max-scale=1">
    <!-- Stylesheets
    ============================================= -->
    <link href="{{ asset('greenergy/css/css-assets.css')}}" rel="stylesheet">
    <link href="{{ asset('greenergy/css/style.css')}}" rel="stylesheet">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700i,700" rel="stylesheet">-->
    <link href="//db.onlinewebfonts.com/c/2090551770be22b09600a40b0b4673b7?family=Avenir+Medium" rel="stylesheet" type="text/css"/>
    <link href="//db.onlinewebfonts.com/c/29aa4b03f27e68cfa927f19d13dc467a?family=Black-" rel="stylesheet" type="text/css"/>

    <!-- Favicon
    ============================================= -->
    <link rel="shortcut icon" href="{{asset('greenergy/images/files/favicon-16X16px.png')}}">


    <style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
    <style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style>
</head>

<body class="device-lg" data-gr-c-s-loaded="true" style="">

<div id="scroll-progress"><div class="scroll-progress" style="width: 0%;"><span class="scroll-percent">0%</span></div></div>

<div class="topheader">
    <div class="toptext" style="color: darkgreen">
        <img src="{{ asset('greenergy/images/files/GREENERGY-320X50.png')}}">
    </div>
</div>

<!-- Document Full Container
============================================= -->
<div id="full-container">

    <!-- Header
    ============================================= -->
    <header id="header">

        <div id="header-bar-1" class="header-bar">

            <div class="header-bar-wrap">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="hb-content">
                                <a class="logo logo-header" href="https://bit.ly/zrdmcwebinar">
                                    <!--<img src="images/files/logo-header-alt.png" alt="">-->
                                </a><!-- .logo end -->
                                <div class="hb-meta">
                                </div><!-- .hb-meta end -->
                            </div><!-- .hb-content end -->

                        </div><!-- .col-md-12 end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->

            </div><!-- .header-bar-wrap -->

        </div><!-- #header-bar-1 end -->

    </header><!-- #header end -->

    <!-- Banner
    ============================================= -->
    <section id="banner">

        <div class="banner-parallax" data-banner-height="800" style="min-height: 800px;">
            <!--<div class="bg-element" data-stellar-background-ratio="0.2" style="background-image: url(&quot;images/files/header-banner.png&quot;); background-position: 50% 0%;">

            </div>-->
            <div class="overlay-colored color-bg-gradient opacity-90"></div><!-- .overlay-colored end -->
            <div class="slide-content">

                <div class="container">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-7" style="min-height: 800px;">

                            <div class="banner-center-box text-white md-text-center">
                                <h1>
                                    Welcome to Greenergy.Me
                                </h1>
                                <i style="color: white">
                                    <h2>Welcome to the best of life.</h2>
                                </i>
                                <br>
                                <div class="description">

                                    We offer high quality handpicked organic fruits and vegetables straight from the farm to your home along with a line-up of ayurvedic products. We use hydroponic, aeroponic methods and zero-pesticides in our organic farming where we save up to 95% of water as compared to regular farming.<br><br>
                                    It's a membership only platform where we make no compromises in the quality of our products and ensure you the most nutritious and healthiest fruits and vegetables in all seasons.<br><br>

                                    <h2>Why Greenergy.Me?</h2>
                                        <ul>
                                            <li>100% Organic. 100% Transparency.</li>
                                            <li>Led by Experts</li>
                                            <li>Farm to Table</li>
                                            <li>Fresh Delivery always.</li>
                                            <li>Designed for everyday nutrition</li>
                                            <li>Sustainable practices</li>
                                        </ul>
                                    </b>
                                </div>
                                <div class="countDown">
                                    We are coming soon to serve you in

                                <h1 id="demo"></h1>
                                </div>
                                <a class="btn-video lightbox-iframe mt-40" href="https://www.youtube.com/watch?v=Q_dhqCSkTT8">
                                    <i class="fa fa-play"></i>
                                    <span class="title">Greenergy says hello!</span>

                                </a><!-- .video-btn end -->

                            </div><!-- .banner-center-box end -->

                        </div><!-- .col-md-7 end -->
                        <div class="col-md-5" style="min-height: 800px;">

                            <div class="banner-center-box text-center text-white">
                                <div class="cta-subscribe cta-subscribe-2 box-form">
                                    <div class="box-title text-white">
                                        <h3 class="title">Want to know more about our offerings?</h3>
                                        <p style="font-size:18px;">Write to us here and we will get in touch with you at your convenient date and time.</p>

                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 962 115" style="enable-background:new 0 0 962 115;" xml:space="preserve" preserveAspectRatio="none" class="svg replaced-svg">
<style type="text/css">
    .st0{fill:#FFFFFF;}
</style>
                                            <path class="st0" d="M0,0c0,0,100,94,481,95C862,94,962,0,962,0v115H0V0z"></path>
</svg>
                                    </div><!-- .box-title end -->
                                    <div class="box-content">
                                        <form method="POST" action="{{route('call-requests.store')}}" id="form-cta-subscribe-2" class="form-inline" novalidate>
                                            @csrf
                                            <div class="cs-notifications">
                                                <div class="cs-notifications-content"></div>
                                                <div id="successMessage"></div>
                                                <div id="errorMessage"></div>
                                            </div><!-- .cs-notifications end -->

                                            <div class="form-group">
                                                <label for="cs2Name">Your Name *</label>
                                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control" placeholder="" required>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div><!-- .form-group end -->

                                            <div class="form-group">
                                                <label for="cs2Email">Your Email *</label>
                                                <input type="email" name="email" id="email" value="{{old('email')}}" class="form-control" placeholder="" required>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div><!-- .form-group end -->

                                            <div class="form-group">
                                                <label for="cs2PhoneNum">Phone Number *</label>
                                                <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control" required maxlength="10">
                                                @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div><!-- .form-group end -->

                                            <div class="form-group">
                                                <label for="cs2Message">City *</label>
                                                <select type="text" name="city" id="city" class="form-control" required>
                                                    <option value="delhi-ncr">Delhi NCR</option>
                                                    <option value="pune">Pune</option>
                                                    <option value="mumbai">Mumbai</option>
                                                    <option value="kolkata">Kolkata</option>
                                                    <option value="jaipur">Jaipur</option>
                                                </select>
                                                @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div><!-- .form-group end -->

                                            <div class="form-group">
                                                <label for="cs2Message">Pincode *</label>
                                                <input type="text" name="pincode" id="pincode" value="{{old('pincode')}}" class="form-control" required maxlength="6">
                                                @error('pincode')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="cs2Message">Convenient Date & Time *</label>
                                                <input type="datetime-local" name="call_time" id="call_time" value="{{old('call_time')}}" class="form-control" required maxlength="6">
                                                @error('call_time')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong style="color: red">{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <input type="submit" class="form-control" value="SUBMIT">
                                            </div><!-- .form-group end -->
                                        </form><!-- #form-cta-subscribe-2 end -->
                                    </div><!-- .box-content end -->
                                </div><!-- .box-form end -->
                            </div><!-- .banner-center-box end -->

                        </div><!-- .col-md-5 end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->

            </div><!-- .slide-content end -->
            <div class="section-separator wave-1 bottom">
                <div class="ss-content">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 310" version="1.1" class="svg replaced-svg">
                        <!-- Generator: Sketch 51.1 (57501) - http://www.bohemiancoding.com/sketch -->
                        <title>Path 7</title>
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="Landing-Pages" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="03_vApp-v3-Copy" transform="translate(0.000000, -554.000000)" fill="#FFFFFF">
                                <path d="M-3,551 C186.257589,757.321118 319.044414,856.322454 395.360475,848.004007 C509.834566,835.526337 561.525143,796.329212 637.731734,765.961549 C713.938325,735.593886 816.980646,681.910577 1035.72208,733.065469 C1254.46351,784.220361 1511.54925,678.92359 1539.40808,662.398665 C1567.2669,645.87374 1660.9143,591.478574 1773.19378,597.641868 C1848.04677,601.75073 1901.75645,588.357675 1934.32284,557.462704 L1934.32284,863.183395 L-3,863.183395" id="Path-7"></path>
                            </g>
                        </g>
                    </svg>
                </div><!-- .ss-content -->
            </div><!-- .section-separator -->
        </div><!-- .banner-parallax end -->

    </section><!-- #banner end -->

</div><!-- #full-container end -->

<a class="scroll-top-icon scroll-top" href="#"><i class="fa fa-angle-up"></i></a>

<!-- External JavaScripts
============================================= -->
<script src="{{asset('greenergy/js/jquery.js')}}"></script>
<script src="{{asset('greenergy/js/call-request.js')}}"></script>
<script src="{{asset('greenergy/js/jRespond.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.fitvids.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.stellar.js')}}"></script>
<script src="{{asset('greenergy/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.mb.YTPlayer.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('greenergy/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('greenergy/js/simple-scrollbar.min.js')}}"></script>
<script src="{{asset('greenergy/js/functions.js')}}"></script>
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("July 15, 2020 00:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("demo").innerHTML = days + "d " + hours + "h: "
            + minutes + "m: " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>
</body>
</html>
