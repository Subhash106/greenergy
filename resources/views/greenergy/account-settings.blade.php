@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">My Wishlist</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">My Wishlist</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!-- account-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-9">
                    <div class="card style-2">
                        <div class="card-header">
                            <h4 class="mb-0">Account Detail</h4>
                        </div>
                        <div class="card-body">
                            <form class="submit-page">
                                <div class="row">

                                    <div class="col-12 col-md-6">
                                        <!-- Email -->
                                        <div class="form-group">
                                            <label>First Name *</label>
                                            <input class="form-control" type="text" placeholder="First Name *" value="Yash" required="">
                                        </div>

                                    </div>

                                    <div class="col-12 col-md-6">
                                        <!-- Last Name -->
                                        <div class="form-group">
                                            <label>Last Name *</label>
                                            <input class="form-control" type="text" placeholder="Last Name *" value="Verma" required="">
                                        </div>

                                    </div>

                                    <div class="col-12">
                                        <!-- Email -->
                                        <div class="form-group">
                                            <label> Email Address *</label>
                                            <input class="form-control" type="email" placeholder="Email Address *" value="yash77@gmail.com" required="">
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <!-- Password -->
                                        <div class="form-group">
                                            <label>Current Password *</label>
                                            <input class="form-control" type="password" placeholder="Current Password *" required="">
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <!-- Password -->
                                        <div class="form-group">
                                            <label>New Password *</label>
                                            <input class="form-control" type="password" placeholder="New Password *" required="">
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <!-- Birthday -->
                                        <div class="form-group">

                                            <label>Date of Birth</label>
                                            <div class="row">
                                                <div class="col-auto">
                                                    <!-- Date -->
                                                    <label class="sr-only">Date</label>
                                                    <select class="form-control">
                                                        <option>09</option>
                                                        <option>10</option>
                                                        <option selected="">11</option>
                                                    </select>
                                                </div>

                                                <div class="col">
                                                    <!-- Date -->
                                                    <label class="sr-only">Month</label>
                                                    <select class="form-control">
                                                        <option>January</option>
                                                        <option>February</option>
                                                        <option selected="">March</option>
                                                    </select>
                                                </div>

                                                <div class="col-auto">
                                                    <!-- Date -->
                                                    <label class="sr-only">Year</label>
                                                    <select class="form-control">
                                                        <option>1990</option>
                                                        <option selected="">1993</option>
                                                        <option>1992</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <!-- Gender -->
                                        <div class="form-group mb-8">
                                            <label>Gender</label>
                                            <div class="btn-group-toggle mt-2 d-flex bd-highlight">
                                                <div class="mr-2">
                                                    <input id="male" class="radio-custom" name="gen" type="radio">
                                                    <label for="male" class="pl-1 radio-custom-label">Male</label>
                                                </div>
                                                <div class="ml-1">
                                                    <input id="female" class="radio-custom" name="gen" type="radio">
                                                    <label for="female" class="pl-1 radio-custom-label">Female</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn2 btn-dark2" type="submit">Save Changes</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
@stop
