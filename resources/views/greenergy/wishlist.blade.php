@extends('layouts.frontend')

@section('content')
    <!-- breadcrumbs -->
    <div class="breadcrumbs_wrapper bg-dark-bd">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <h2 class="bd_title">My Wishlist</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb_item">
                                    <a href="#"><i class="ti-home"></i></a>
                                </li>
                                <li class="breadcrumb_item">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="breadcrumb_item active" aria-current="page">My Wishlist</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

    <!-- wishlist-section -->
    <section class="gray order-section">
        <div class="container">
            <div class="row">
                @include('greenergy.partials.sidebar-menu')
                <div class="col-lg-8 col-md-9">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <span class="pr_tag hot">Hot</span>
                                <div class="product_thumb">
                                    <img src="images/vegetable1.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <!-- <span class="pr_tag hot">Hot</span> -->
                                <div class="product_thumb">
                                    <img src="images/vegetable2.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <!-- <span class="pr_tag new">New</span> -->
                                <div class="product_thumb">
                                    <img src="images/vegetable3.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <span class="pr_tag new">New</span>
                                <div class="product_thumb">
                                    <img src="images/vegetable4.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <span class="offer_sell">Save 20% Off</span>
                                <div class="product_thumb">
                                    <img src="images/vegetable5.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="product_grid">
                                <div class="product_thumb">
                                    <img src="images/vegetable6.png" class="img-fluid" alt="">
                                </div>
                                <div class="product_caption center">
                                    <div class="rate">
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star filled"></i>
                                        <i class="ti-star"></i>
                                    </div>
                                    <div class="title">
                                        <h4 class="pro_title"><a href="detail-1.html">Accumsan Tree Fusce</a></h4>
                                    </div>
                                    <div class="price">
                                        <h6>&#x20B9;72.47<span class="less_price">&#x20B9;112.10</span></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item left">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true"><i class="ti-arrow-left mr-1"></i>Prev</span>
                                        </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item right">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true"><i class="ti-arrow-right mr-1"></i>Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end -->
@stop
