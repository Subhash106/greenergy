<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('greenergy-latest/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('greenergy-latest/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('greenergy-latest/css/fullpage.css') }}">
    <link rel="stylesheet" href="{{ asset('greenergy-latest/css/main.css') }}">
</head>
<body class="loading">
<header id="myHeader" class="header-inner">
    <nav class="navbar navbar-expand-sm navbar-light bg-white">
        <div class="container">
            <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
              aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button> -->
            <a class="navbar-brand logo" href="#"><span class="logo-icon"><img src="{{ asset('greenergy-latest/images/green-icon.png') }}"></span><span><img
                        src="{{ asset('greenergy-latest/images/green.png') }}"></span></a>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">


                <!-- <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                  </li>
                </ul> -->
            </div>
            <ul class="header-inner navbar">
                <li><a href="{{ url('/cart') }}" class="cart_btn text-black"><i class="ti-shopping-cart"></i></a></li>
                <li><a href="{{ url('/my-order') }}" class="text-black"><i class="ti-user"></i></a></li>
                <li><a href="{{ url('/wallet') }}" class="text-black"><i class="ti-wallet"></i></a></li>
            </ul>
        </div>
    </nav>
</header>

@yield('content')


<!-- java-script -->
<script src="{{ asset('greenergy-latest/js/jquery-min.js') }}"></script>
<script src="{{ asset('greenergy-latest/js/fullpage.js') }}"></script>
<script src="{{ asset('greenergy-latest/js/slim-min.js') }}"></script>
<script src="{{ asset('greenergy-latest/js/popper-min.js') }}"></script>
<script src="{{ asset('greenergy-latest/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('greenergy-latest/js/custom.js') }}"></script>
<!-- end -->

</body>
</html>
