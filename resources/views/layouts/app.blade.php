<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    GREENERGY
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                        {{--<li class="nav-item @if(request()->getPathInfo() == '/admin/call_requests') active @endif ">
                            <a class="nav-link" href="{{ url('/admin/call_requests') }}">
                                Call Requests
                            </a>
                        </li>--}}
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/categories') active @endif">
                            <a class="nav-link" href="{{ url('/admin/categories') }}">
                                Categories
                            </a>
                        </li>
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/products') active @endif">
                            <a class="nav-link" href="{{ url('/admin/products') }}">
                                Products
                            </a>
                        </li>
                        {{--<li class="nav-item @if(request()->getPathInfo() == '/admin/referrals') active @endif">
                            <a class="nav-link" href="{{ url('/admin/referrals') }}">
                                Referrals
                            </a>
                        </li>--}}
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/users') active @endif">
                            <a class="nav-link" href="{{ url('/admin/users') }}">
                                Users
                            </a>
                        </li>
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/roles') active @endif">
                            <a class="nav-link" href="{{ url('/admin/roles') }}">
                                Roles
                            </a>
                        </li>
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/permissions') active @endif">
                            <a class="nav-link" href="{{ url('/admin/permissions') }}">
                                Permissions
                            </a>
                        </li>
                        {{--<li class="nav-item @if(request()->getPathInfo() == '/admin/gold-prices') active @endif">
                            <a class="nav-link" href="{{ url('/admin/gold-prices') }}">
                                Gold Price
                            </a>
                        </li>
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/banners') active @endif">
                            <a class="nav-link" href="{{ url('/admin/banners') }}">
                                Banners
                            </a>
                        </li>
                        <li class="nav-item @if(request()->getPathInfo() == '/admin/product-call-request-list') active @endif">
                            <a class="nav-link" href="{{ url('/admin/product-call-request-list') }}">
                                Product Call Requests
                            </a>
                        </li>--}}
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                @endif
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @yield('content')
        </main>
    </div>
@stack('scripts')
</body>
</html>
