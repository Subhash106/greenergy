<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/.well-known/pki-validation/F34F3664E3D4504ED609FF340B723EBD.txt', function() {
    $path = base_path() . '/.well-known/pki-validation/F34F3664E3D4504ED609FF340B723EBD.txt';
    header('Content-Type: text/plain');
    header('Content-Disposition: inline; filename="F34F3664E3D4504ED609FF340B723EBD.txt"');
    readfile($path);
});

Route::get('/', 'FrontendController@index');

Route::get('/faq',  'FrontendController@faq');

Route::get('/account-settings',  'FrontendController@accountSettings');

Route::get('/cart',  'FrontendController@cart');

Route::get('/my-order',  'FrontendController@myOrder');

Route::get('/order-history',  'FrontendController@orderHistory');

Route::get('/order-tracking',  'FrontendController@orderTracking');

Route::get('/payment-methods',  'FrontendController@paymentMethods');

Route::get('/wallet',  'FrontendController@wallet');

Route::get('/wishlist',  'FrontendController@wishlist');

Route::get('/product/{product}',  'FrontendController@getProduct')->name('get-product');

/*Route::get('/', function () {
    return view('greenergy.greenergy');
});*/

Route::namespace('Greenergy')->prefix('admin')->group(function (){
    Route::get('call_requests', 'CallRequestsController@index')->name('call-requests.index')->middleware('auth');
    Route::post('call-requests', 'CallRequestsController@store')->name('call-requests.store');
});

Route::namespace('Admin')->prefix('admin')->middleware('auth')->group(function () {
    Route::resource('categories', 'CategoriesController');
    Route::post('categories/get-category-by-main-category', 'CategoriesController@getCategoryByMainCategory')
        ->name('get-categories-by-main-category-id');
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::post('assign-to-roles/{role_id}', 'PermissionsController@assignToRoles')->name('assign-to-roles');
    Route::delete('remove-role/{id}', 'PermissionsController@removeRole')->name('remove-from-role');
    Route::post('assign-permissions/{permission_id}', 'RolesController@assignPermissions')->name('assign-permissions');
    Route::delete('remove-permission/{id}', 'RolesController@removePermission')->name('remove-permission');
    Route::resource('products', 'ProductsController');
    Route::post('products/add-packages/{product}', 'ProductsController@addPackages')->name('add-packages');
    Route::delete('products/remove-package/{product}', 'ProductsController@removePackage')->name('remove-package');
    Route::get('download-excel-template', 'ProductsController@downloadExcelTemplate')->name('download-excel-template');
    Route::get('products/upload/bulk-products', 'ProductsController@bulkUpload')->name('products.bulk-upload');
    Route::post('products/store/bulk-products', 'ProductsController@storeBulkUpload')->name('products.store-bulk-upload');
    Route::post('products/publish/{id}', 'ProductsController@publish')->name('products.publish');
    Route::post('products/unpublish/{id}', 'ProductsController@unPublish')->name('products.unpublish');

    //Referrals routes
    Route::resource('referrals', 'ReferralsController');

    //Users routes
    Route::get('users', 'UsersController@index')->name('users.index');
    Route::patch('users/{id}/enable', 'UsersController@enable')->name('users.enable');
    Route::patch('users/{id}/disable', 'UsersController@disable')->name('users.disable');
    Route::get('users/{user}', 'UsersController@details')->name('users.details');


    // Gold Price route
    Route::get('gold-prices', 'GoldPriceController@index')->name('gold-price-list');
    Route::get('gold-prices/create', 'GoldPriceController@create')->name('gold-price.create');
    Route::post('gold-prices', 'GoldPriceController@store')->name('gold-price.store');

    //Banner Routes
    Route::resource('banners', 'BannersController');
    Route::put('banners/status-change/{banner}','BannersController@changeStatus')->name('banner-status-change');

    //Product Call Request
    Route::get('product-call-request-list', 'ProductCallRequestsController@callRequestList')->name('product-call-request-list');


});


//Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
