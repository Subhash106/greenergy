<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'middleware' => 'auth:api',
    'namespace' => '\App\Http\Controllers\API',
    'prefix' => 'auth'
], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group([
    //'middleware' => 'auth:api',
    'namespace' => '\App\Http\Controllers\API',
], function ($router) {
    Route::resource('products', 'ProductsController')->only('index');
    Route::get('products/details/{id}/{userId}', 'ProductsController@details');
    Route::get('search-product', 'ProductsController@searchProduct')->name('products.search-product');
    Route::get('products-featured/{id}', 'ProductsController@featured');
    Route::resource('banners', 'BannersController')->only('index');
    Route::get('categories/{id}/{userId}', 'CategoriesController@index');
    Route::get('maincategories', 'MainCategoriesController@index');
    Route::get('categories/{id}/products/{userId}', 'CategoriesController@products');

    Route::post('send-call-request', 'ProductCallRequestsController@sendCallRequest');
    Route::get('call-requests-by-user/{id}', 'ProductCallRequestsController@CallbackRequestsByUser');

//============== wish list ======================================
    Route::post('add-product-to-wishlist', 'WishListController@addProduct');
    Route::delete('remove-product-from-wishlist', 'WishListController@removeProduct');
    Route::get('get-all-wishlist-products/{user_id}', 'WishListController@getAllProducts');
});

Route::group([
    'namespace' => '\App\Http\Controllers\API',
    'prefix' => 'auth'
], function (){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
});

//=============== Otp Sign In & Sign Up Process ==================

Route::group([
    'namespace' => '\App\Http\Controllers\API',
    'prefix' => 'auth'
], function (){
    Route::post('get-otp', 'AuthOtpController@getOtp');
    Route::post('verify-otp-to-login', 'AuthOtpController@verifyOtpToLogin');
    Route::post('sign-up', 'AuthOtpController@signUp');
});
