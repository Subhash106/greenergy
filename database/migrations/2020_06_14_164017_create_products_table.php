<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->double('price');
            $table->double('selling_price')->nullable()->default(300);
            $table->unsignedBigInteger('category_id');
            $table->text('ingredients')->nullable();
            $table->float('energy')->nullable()->default(0.00);
            $table->float('protein')->nullable()->default(0.00);
            $table->float('dietary_fiber')->nullable()->default(0.00);
            $table->float('carbohydrates')->nullable()->default(0.00);
            $table->float('sugar')->nullable()->default(0.00);
            $table->float('total_fat')->nullable()->default(0.00);
            $table->float('saturated_fat')->nullable()->default(0.00);
            $table->float('fiber')->nullable()->default(0.00);
            $table->float('calories')->nullable()->default(0.00);
            $table->text('custom_fields')->nullable();
            $table->timestamps();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
