<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\Admin\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('role_user')->truncate();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        $super_admin_id = DB::table('users')->insertGetId([
            'name' => 'Administrator',
            'email' => 'super_admin@mbj.in',
            'phone' => 9988665522,
            'password' => Hash::make('Admin@123#')
        ]);

        $admin_id = DB::table('users')->insertGetId([
            'name' => 'Admin',
            'email' => 'admin@mbj.in',
            'phone' => 9988665511,
            'password' => Hash::make('Admin@123#')
        ]);

        $roles = Role::all();
        $super_admin_role_id = null;
        $admin_role_id = null;
        foreach ($roles as $role) {
            if ($role->slug == 'super-admin')
                $super_admin_role_id = $role->id;
            if ($role->slug == 'admin')
                $admin_role_id = $role->id;
        }

        DB::table('role_user')->insert([
                ['role_id' => $super_admin_role_id, 'user_id' => $super_admin_id],
                ['role_id' => $admin_role_id, 'user_id' => $admin_id]
            ]);

    }
}
