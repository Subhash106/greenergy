<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    public function run(){
        $permission = \App\Models\Admin\Permission::where('slug','can-publish-product')->first();
        if(!$permission)
        {
            DB::table('permissions')->insert([
                'permission' => 'Can Publish Product',
                'slug' => 'can-publish-product',
            ]);
        }
    }
}
