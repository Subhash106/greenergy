<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    public function run(){
        Schema::disableForeignKeyConstraints();
        DB::table('roles')->truncate();
        Schema::enableForeignKeyConstraints();
        DB::table('roles')->insert([
            ['role' => 'Super Admin', 'slug' => 'super-admin'],
            ['role' => 'Admin', 'slug' => 'admin']
        ]);

/*
        $role = Role::where('slug','super-admin')->first();
        $permission = Permission::where('slug','can-publish-product')->first();

        DB::table('permission_role')->insert(['role_id'=>$role->id, 'permission_id'=>$permission->id]);*/
    }
}
