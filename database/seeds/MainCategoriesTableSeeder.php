<?php

use Illuminate\Database\Seeder;

class MainCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('main_categories')->truncate();
        Schema::enableForeignKeyConstraints();

        Schema::disableForeignKeyConstraints();
        DB::table('categories')->truncate();
        Schema::enableForeignKeyConstraints();

        $id = DB::table('main_categories')->insertGetId([
            'name' => 'From The Farm',
            'slug' => 'from-the-farm'
        ]);

        DB::table('categories')->insert([
            ['name' => 'Vegetables', 'main_category_id' => $id, 'video_name' => 'Vegetables.mp4'],
            ['name' => 'Leafy vegetables', 'main_category_id' => $id, 'video_name' => 'Leafy Vegetables.mp4'],
        ]);

        $id = DB::table('main_categories')->insertGetId([
            'name' => 'Organic Products',
            'slug' => 'organic-products'
        ]);


        DB::table('categories')->insert([
            ['name' => 'Flours', 'main_category_id' => $id, 'video_name' => 'Atta.mp4'],
            ['name' => 'Food Grains', 'main_category_id' => $id, 'video_name' => 'food-grains.mp4'],
            ['name' => 'Sweetener', 'main_category_id' => $id, 'video_name' => 'Sweetner.mp4'],
            ['name' => 'Lentils', 'main_category_id' => $id, 'video_name' => 'Pulses.mp4'],
            ['name' => 'Oil & Ghee', 'main_category_id' => $id, 'video_name' => 'Oil1.mp4'],
            ['name' => 'Spices', 'main_category_id' => $id, 'video_name' => 'Spices.mp4'],
        ]);

        DB::table('products')->insert([
            ['category_id' => 1, 'name' => 'Potato', 'price' => 250],
            ['category_id' => 1, 'name' => 'Onion', 'price' => 250],
            ['category_id' => 1, 'name' => 'Garlic', 'price' => 250],
            ['category_id' => 1, 'name' => 'Ginger', 'price' => 250],
            ['category_id' => 1, 'name' => 'Torai', 'price' => 250],
            ['category_id' => 1, 'name' => 'Lauki', 'price' => 250],
            ['category_id' => 1, 'name' => 'Gwar Ki Phali', 'price' => 250],
            ['category_id' => 1, 'name' => 'Palak', 'price' => 250],
            ['category_id' => 1, 'name' => 'Green Chilli', 'price' => 250],
            ['category_id' => 1, 'name' => 'Dhania', 'price' => 250],
            ['category_id' => 1, 'name' => 'Mint', 'price' => 250],
            ['category_id' => 2, 'name' => 'Lettuce -Green', 'price' => 250],
            ['category_id' => 2, 'name' => 'Lettuce -Red', 'price' => 250],
            ['category_id' => 2, 'name' => 'Lettuce -Romaine', 'price' => 250],
            ['category_id' => 2, 'name' => 'Lettuce -Butterhead', 'price' => 250],
            ['category_id' => 2, 'name' => 'Lettuce -Batavia', 'price' => 250],
            ['category_id' => 2, 'name' => 'Kale', 'price' => 250],
            ['category_id' => 2, 'name' => 'Red Amaranthus', 'price' => 250],
            ['category_id' => 2, 'name' => 'Italian Basil', 'price' => 250],
            ['category_id' => 2, 'name' => 'Thai Basil', 'price' => 250],
            ['category_id' => 2, 'name' => 'Purple Basil', 'price' => 250],
            ['category_id' => 2, 'name' => 'Arugula /Rocket', 'price' => 250],
            ['category_id' => 3, 'name' => 'Wheat Flour', 'price' => 250],
            ['category_id' => 3, 'name' => 'Corn Flour', 'price' => 250],
            ['category_id' => 3, 'name' => 'Besan', 'price' => 250],
            ['category_id' => 4, 'name' => 'Basmati Rice White', 'price' => 250],
            ['category_id' => 4, 'name' => 'Rusi Rice', 'price' => 250],
            ['category_id' => 4, 'name' => 'Sama Rice', 'price' => 250],
            ['category_id' => 4, 'name' => 'Bajra', 'price' => 250],
            ['category_id' => 4, 'name' => 'Jwar', 'price' => 250],
            ['category_id' => 5, 'name' => 'Triple Refined Sugar', 'price' => 250],
            ['category_id' => 5, 'name' => 'Desi Sugar (Khand)', 'price' => 250],
            ['category_id' => 5, 'name' => 'Jaggery', 'price' => 250],
            ['category_id' => 5, 'name' => 'Honey-Silver/Gold', 'price' => 250],
            ['category_id' => 6, 'name' => 'Arhar Dal', 'price' => 250],
            ['category_id' => 6, 'name' => 'Moong Dal Dhuli', 'price' => 250],
            ['category_id' => 6, 'name' => 'Chana Dal', 'price' => 250],
            ['category_id' => 6, 'name' => 'Urad Dal', 'price' => 250],
            ['category_id' => 6, 'name' => 'Matar Dal', 'price' => 250],
            ['category_id' => 6, 'name' => 'Masoor Dal', 'price' => 250],
            ['category_id' => 7, 'name' => 'Sunflower Oil', 'price' => 250],
            ['category_id' => 7, 'name' => 'Mustard Oil', 'price' => 250],
            ['category_id' => 7, 'name' => 'Til Oil', 'price' => 250],
            ['category_id' => 7, 'name' => 'A2 Ghee', 'price' => 250],
            ['category_id' => 8, 'name' => 'Dhaniya Powder', 'price' => 250],
            ['category_id' => 8, 'name' => 'Haldi Powder', 'price' => 250],
            ['category_id' => 8, 'name' => 'Saunf', 'price' => 250],
        ]);



    }
}
