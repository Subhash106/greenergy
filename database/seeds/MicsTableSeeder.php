<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Category;
use App\Models\Admin\State;
use App\Models\Admin\BannerCategory;
use Illuminate\Support\Facades\DB;

class MicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();
        DB::table('states')->insert([
            ['state'=>'ANDAMAN & NICOBAR'],
            ['state'=>'ANDHRA PRADESH'],
            ['state'=>'ARUNACHAL PRADESH'],
            ['state'=>'ASSAM'],
            ['state'=>'BIHAR'],
            ['state'=>'CHANDIGARH'],
            ['state'=>'CHATTISGAR'],
            ['state'=>'DADRA & NAGAR'],
            ['state'=>'DAMAN & DIU'],
            ['state'=>'DELHI'],
            ['state'=>'GOA'],
            ['state'=>'GUJRAT'],
            ['state'=>'HARYANA'],
            ['state'=>'HIMACHAL PRADESH'],
            ['state'=>'JAMMU & KASHMIR'],
            ['state'=>'JHARKHAND'],
            ['state'=>'KARNATAKA'],
            ['state'=>'KERALA'],
            ['state'=>'LAKSHDWEEP'],
            ['state'=>'MADHYA PRADESH'],
            ['state'=>'MAHARASHTRA'],
            ['state'=>'MANIPUR'],
            ['state'=>'MEGHALAYA'],
            ['state'=>'MIZORAM'],
            ['state'=>'NAGALAND'],
            ['state'=>'ORISSA'],
            ['state'=>'PONDICHERY'],
            ['state'=>'PUNJAB'],
            ['state'=>'RAJASTHAN'],
            ['state'=>'SIKKIM'],
            ['state'=>'TAMIL NADU'],
            ['state'=>'TRIPURA'],
            ['state'=>'UTTAR PRADESH'],
            ['state'=>'UTTARANCHAL'],
            ['state'=>'WEST BENGAL']
        ]);

        $banner_category = BannerCategory::where('name','Upcoming New Design')->Orwhere('name','New Trends')->get();
        if($banner_category->count()==0)
            DB::table('banner_categories')->insert([
               ['name'=>'Upcoming New Design'],['name'=>'New Trends']
            ]);
    }
}
